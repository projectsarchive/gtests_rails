module UsersHelper

  # Returns the Gravatar (http://gravatar.com/) for the given user.
  def gravatar_for(user)
    url = user.image
    if user.email
      gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
      url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
    end
    image_tag(url, alt: user.full_name, class: 'img-rounded')
  end
end