module Training::QuestionsHelper

  def completion_class_for question
    "icons-completion-#{(question.percentage / 25 ).round * 25}#{'-new' unless question.started}"
  end

  def completion_icon_for question, icon_class=''
    "<span class=\"completion #{completion_class_for(question)} #{icon_class}\">&nbsp;</span>"
  end

  def question_path question, params={}
    category_question_path question.category, question, params
  end

  def answer_questions_question_path(question)
    category_question_path(question.category, question)
  end

  def view_questions_question_path(question)
    view_category_question_path(question.category, question)
  end

end
