module Training::DisksHelper
  def finish_attempt_path(attempt)
    finish_category_disk_path(attempt.disk.category, attempt.disk)
  end

  def attempt_path(attempt)
    attempt_category_disk_path(attempt.disk.category, attempt.disk, attempt)
  end

  def answer_disk_question_path(question)
    disk = question.disk
    test_category_disk_path(disk.category, disk, question_number(question))
  end

  def view_disk_question_path(question, attempt=nil)
    params = {}
    if attempt
      params[:attempt] = attempt.id
      answer = get_answer(question, attempt)
      params[:answer] = answer.id if answer
    end
    view_category_disk_path(question.category, question.disk, question_number(question), params)
  end

  def questions_array(disk)
    Kaminari.paginate_array(disk.questions).page(params[:question_number]).per(1)
  end

  def question_number(question)
    question.disk.questions.index(question) + 1
  end

  def total_questions(question)
    question.disk.questions.size
  end

  def answer_text_for_question(question, user_attempt)
    answer = get_answer(question, user_attempt)
    answer ? answer.text : 'Не отвечено'
  end

  def answer_class(question, user_attempt)
    answer = get_answer(question, user_attempt)
    return 'warning' unless answer
    answer.correct ? 'success' : 'danger'
  end

  def next_disk_link(disk, options = {})
    if disk.category.next_disk(disk)
      link_to 'Пройти следующий тест >', category_disk_path(disk.category, disk.category.next_disk(disk)), options
    end
  end

  def attempt_results(attempt)
    "#{attempt.correct_count || 0} из #{attempt.disk.questions.count}"
  end

  def requested_attempt_link
    if params[:attempt]
      link_to '<- Вернуться к результатам теста',
              attempt_category_disk_path(@question.category, @question.disk, params[:attempt])
    end
  end

  def disk_score_percentage(score, disk)
    percentage = [(100 * score / disk.questions.count / 10.0).ceil * 10, 99].min
    "#{percentage}%"
  end

  private

  def get_answer(question, user_attempt)
    user_answer = user_attempt.user_answers.where(question: question).first
    user_answer.answer if user_answer
  end
end
