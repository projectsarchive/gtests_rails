module ApplicationHelper
  def title(page_title)
    content_for(:title) { page_title.to_s }
  end

  def flash_class(level)
    case level.to_sym
      when :notice then 'info'
      when :error then 'danger'
      when :alert then 'warning'
      when :success then 'success'
        else 'info' + " #{level}"
    end
  end

  def admin?
    current_user.try(:admin?)
  end

  def current_user_money(params = {})
    no_button = params[:no_button]
    if current_user
      content_tag(:span, class: "points-area #{params[:class]}") do
        tooltip_data = { toggle: 'tooltip', placement: 'bottom', delay: '{"hide":"1000"}', html: true}
        faq_link = link_to '"Вопросы и ответы"', page_faq_path(anchor: 'points-system')
        tooltip_text = "Ваш баланс баллов в системе. Подробнее можно прочитать в разделе #{faq_link}"
        concat content_tag(:span, ' ', class: 'icons-coin', title: tooltip_text, data: tooltip_data)
        concat(
            capture do
              content_tag(:span, class: 'points-info') do
                # Actual update of the points balance is performed in Merit::BalanceUpdater (lib) in after_action method
                # It's done this way because points are updated after the controller prepares the output
                '-1 балл'
              end
            end
        )
        concat link_to('Пополнить', payments_new_path, class: 'btn btn-success btn-sm pull-right') unless no_button
      end
    end
  end

end
