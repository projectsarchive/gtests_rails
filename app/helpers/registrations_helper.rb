module RegistrationsHelper
  def has_password_errors? resource
    !(resource.errors.messages.keys & [:password, :password_confirmation, :current_password]).empty?
  end

  def login_provider_text provider
    "#{@prefix ||= 'Войти через'} #{provider.to_s.titleize}"
  end
end
