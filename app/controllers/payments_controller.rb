class PaymentsController < ApplicationController
  protect_from_forgery except: :callback
  before_filter :authenticate_user!, except: [:callback]
  before_filter :check_callback_hash, only: :callback

  def callback
    full_params = payment_params rescue return
    puts full_params.inspect
    payment = Payment.find_or_initialize_by(user: User.find_by(id: full_params['user_id']), operation_id: full_params['operation_id'])
    unless payment.update_attributes(full_params)
      logger.error("YandexMoney callback: unable to update attributes for payment. Params: #{full_params.inspect}. Error: #{payment.errors.full_messages}")
    end
  end

  def new
  end

  private

  def check_callback_hash
    params_to_check = params.merge({notification_secret: AppSettings.YANDEX_MONEY_SECRET})
    final_params = []
    %w(notification_type operation_id amount currency datetime sender codepro notification_secret label).each do |field|
      final_params << params_to_check[field]
    end

    head :fobidden unless Digest::SHA1.hexdigest(final_params.join('&')) == params['sha1_hash']

  end

  def payment_params
    label = JSON.parse(params['label'])
    user = User.find_by(id: label['user'].to_i)
    unless user
      logger.error("YandexMoney callback: user with id #{label['user'].to_i} not found. Label: '#{params['label']}'")
      throw Exception.new
    end
    full_params = params.merge({'user_id' => user.id})
    puts full_params.inspect
    full_params.permit(:notification_type, :operation_id, :amount, :withdraw_amount, :datetime, :sender, :codepro, :label, :user_id)
  end
end
