class SessionsController < Devise::OmniauthCallbacksController
  def all
    identity = Identity.from_omniauth(request.env['omniauth.auth'])
    user = identity.find_or_create_user(current_user)

    if user.valid? || user.new_record?
      ActiveSupport::Notifications.instrument('users.create', :user_id => user.id) if user.is_new?
      flash.notice = t('devise.sessions.signed_in')
      @after_sign_in_path = edit_user_registration_path if user == current_user
      sign_in_and_redirect user
    else
      flash.notice = t('devise.registrations.complete_registration')
      sign_in user
      redirect_to edit_user_registration_url
    end
    #render text: request.env['omniauth.auth'].inspect
  end

  (AppSettings.auth_providers || []).each do |name, options|
    alias_method name.to_sym, :all
  end

  private

  def after_sign_in_path_for(resource)
    back_url = (session[:user_return_to].nil?) ? "/" : session[:user_return_to].to_s
    @after_sign_in_path || back_url || edit_user_registration_path
  end
end
