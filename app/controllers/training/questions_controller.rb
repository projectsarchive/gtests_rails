class Training::QuestionsController < Training::BaseController
  include QuestionsTraining

  before_filter :authenticate_user!
  before_filter :set_question, except: [:index]
  before_filter :set_user_answer, only: [:show, :answer]
  before_filter :prepare_question_view_info, only: [:show, :view, :answer]
  before_action :check_points_balance!, only: [:show, :view, :answer]

  def index
    @questions = filtered_questions.page(params[:page]).per(30)
  end

  def show
  end

  def view

  end

  def answer
    # old_questions_in_group = filtered_questions.ids
    next_question = filtered_questions.next_for(@question).limit(1).first
    updated = @user_answer.update_attributes(user_answer_params)
    @question_view_info.answer = @user_answer.answer
    if updated
      @question.percentage!
      # @question_view_info.find_next_question_within! old_questions_in_group
      @question_view_info.next_question = next_question

      ActiveSupport::Notifications.instrument('question.user_answered', user_answer_id: @user_answer.id)

      respond_to do |format|
        format.js { @question_view_info.options[:remote] = true }
        format.html do
          # redirect_to view_category_question_path(@category, @question, answer: @question_view_info.answer, params: @question_view_info.params)
          render 'training/questions/view'
        end
      end
    else
      respond_to do |format|
        format.js do
          render :json => { :error => @user_answer.errors.full_messages.to_sentence },
                 :status => :unprocessable_entity
        end
        format.html do
          flash[:errors] = @user_answer.errors.full_messages.to_sentence
          redirect_to category_question_path(@category, @question, params: @question_view_info.params)
        end
      end
    end
  end

  protected

  # Needed for Merit, otherwise it gets Question
  def target_object
    @user_answer
  end

  private

  def controller_index_path
    category_questions_path(@category)
  end

  def set_question
    @question = UserQuestion.user(current_user).where(category: @category, id: params[:id]).first!
  end

  def set_user_answer
    @user_answer = UserAnswer.find_or_initialize_by(user: current_user, question: @question, answer_id: nil)
  end

  def prepare_question_view_info
    @question_view_info = QuestionViewInfo.new(@question)
    @question_view_info.params[:filter] = params[:filter]
    respond_to do |format|
      format.js { @question_view_info.options[:remote] = true }
      format.html {}
    end
  end

  def filtered_questions
    questions = UserQuestion.user(current_user).includes(:category).where(category: @category)
    allowed_filters = %w(new learning learned starred)
    if allowed_filters.include? params[:filter]
      scope = "user_#{params[:filter]}"
      questions = questions.send(scope) if questions.respond_to? scope
    end
    questions
  end

  def check_points_balance!
    if current_user.points(category: Merit::Points::MONEY) <= 0
      # See http://stackoverflow.com/questions/5454806/rails-3-how-to-redirect-to-in-ajax-call
      flash[:notice] = 'Для выполнения действия необходим положительный баланс бонусов'
      flash.keep(:notice)
      respond_to do |format|
        format.html { redirect_to new_payment_path }
        format.js { render js: "window.location = '#{new_payment_path}'" }
      end
    end
  end
end
