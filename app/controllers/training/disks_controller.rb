class Training::DisksController < Training::BaseController
  #add_breadcrumb 'Главная', :root_path

  include QuestionsTraining

  before_filter :authenticate_user!, except: [:index]

  before_filter :set_disk, except: [:index]
  before_filter :add_breadcrumb_disk, except: [:index]
  before_filter :set_question, except: [:index, :show, :finish, :attempt, :history]
  before_filter :set_user_attempt, except: [:index, :view, :attempt, :history]
  before_filter :set_user_answer, only: [:test, :test_answer]

  def index
    @disks = @category.disks.with_questions.page(params[:page]).per(10)
    @scores = UserAttempt.best_scores(current_user, @disks.to_a) if current_user
  end

  def show
    question_number = @attempt.next_question_number
    if question_number
      redirect_to test_category_disk_path(@category, @disk, question_number: question_number)
    end
  end

  def test
  end

  def test_answer
    if @user_answer.update_attributes(user_answer_params)
      if @attempt.remains_questions_count > 0
        redirect_to path_for_question @attempt.next_question_number(@question_index + 1)
      else
        redirect_to finish_category_disk_path @category, @disk
      end
    else
      flash[:error] = @user_answer.errors.full_messages.to_sentence
      render 'training/disks/test'
    end
  end

  def finish
    if @attempt.new_record?
      flash[:notice] = 'Отсутствует незавершенная попытка'
      if UserAttempt.where(user: current_user, disk: @disk).count > 0
        redirect_to history_category_disk_path(@category, @disk)
      else
        redirect_to test_category_disk_path(@category, @disk, 1)
      end
    else
      @attempt.finish
      redirect_to attempt_category_disk_path(@category, @disk, @attempt, finish: true)
    end
  end

  def attempt
    add_breadcrumb 'Детально'
    @attempt = UserAttempt.includes(disk: [:questions => [:category]]).find(params[:attempt_id])
    @user = @attempt.user unless @attempt.user == current_user
  end

  def history
    add_breadcrumb 'Результаты'
    @attempts = @disk.user_attempts.where(user: current_user).order('completed_at desc')
  end

  def view
    answer = Answer.find_by(id: params[:answer])
    @question_view_info = QuestionViewInfo.new(@question, answer: answer)
  end

  protected

  # Needed for Merit, otherwise it gets Question
  def target_object
    @user_answer
  end

  private

  def set_disk
    @disk = Disk.find(params[:id])
  end

  def add_breadcrumb_disk
    add_breadcrumb 'По дискам', category_disks_path(@category)
    add_breadcrumb @disk.name_in_lists, category_disk_path(@category, @disk)
  end

  def set_question
    questions_count = @disk.questions.count
    @question_index = params[:question_number].to_i - 1
    if questions_count == 0
      redirect_to category_disk_path(@category, @disk)
    elsif @question_index < 0
      redirect_to path_for_question 1
    elsif @question_index >= questions_count
      redirect_to path_for_question questions_count
    end
    @question = @disk.questions.includes(:category, :answers).to_a[@question_index]
  end

  def set_user_attempt
    @attempt = UserAttempt.get_current_by_user_disk(current_user, @disk)
  end

  def set_user_answer
    attributes = {user: current_user, user_attempt: @attempt, question: @question}
    @user_answer = UserAnswer.where.not(user_attempt_id: nil).where(attributes).first
    @user_answer = UserAnswer.new(user: current_user, user_attempt: @attempt, question: @question) unless @user_answer
  end

  def path_for_question(question_number)
    url_for only_path: true,
            action: action_name,
            category_id: @category.slug,
            id: @disk.id,
            question_number: question_number
  end

  def controller_index_path
    category_disks_path(@category)
  end
end
