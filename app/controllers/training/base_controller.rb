class Training::BaseController < ApplicationController
  before_action :set_category
  before_filter :add_breadcrumb_category

  private

  def set_category
    @category = Category.find(params[:category_id])
  end

  def add_breadcrumb_category
    add_breadcrumb 'Тесты 1С', categories_path
    add_breadcrumb @category.name.truncate(60), category_path(@category)
  end

  def controller_index_path
  end
end