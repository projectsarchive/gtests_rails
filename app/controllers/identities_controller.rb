class IdentitiesController < ApplicationController
  def destroy
    Identity.find(params[:id]).destroy
    redirect_to :back
  end
end
