module QuestionsTraining
  extend ActiveSupport::Concern

  private

  def user_answer_params
    params.fetch(:user_answer, {}).permit(:answer_id)
  end

end