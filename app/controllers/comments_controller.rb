require 'rss'
class CommentsController < ApplicationController
  def recent
    @no_merit_update = true
    if AppSettings['redirect_domains']
      domains = AppSettings['redirect_domains']
      domains = domains.join('|') if domains.is_a? Array
    end
    @disqus_recent_comments = Rails.cache.fetch('/comments/recent', expires_in: 10.minutes) do
      begin
        items = RSS::Parser.parse(open("http://#{AppSettings.disqus_shortname}.disqus.com/latest.rss").read, false).items
        host = request.host
        items.keep_if{ |i| i.link.include? host }.map{|i| i.link.gsub!(/^#{domains}\.#{host}/, host) if(domains) ; i}.take(10)
      rescue Exception => e
        logger.error "Getting RSS of comments for '#{AppSettings.disqus_shortname}.disqus.com': #{e.message}"
        break # See http://stackoverflow.com/questions/19969776/handling-errors-in-rails-cache-fetch
      end
    end
    render layout: false
  end
end
