class CategoriesController < ApplicationController
  before_action :set_category, only: [:show]

  add_breadcrumb 'Главная', :root_path

  # GET /categories
  # GET /categories.json
  def index
    add_breadcrumb 'Тесты 1С'
    @categories = Category.all
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    add_breadcrumb 'Тесты 1С', categories_path
    add_breadcrumb @category.name
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end
end
