class WelcomeController < ApplicationController
  #add_breadcrumb 'Home', '#', :title => 'Back to the Index'

  def index
    #add_breadcrumb 'Index', '/', :title => 'Back to the Index'
    @categories = Category.all
    render locals: { no_ads: true }
  end
end
