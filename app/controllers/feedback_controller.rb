class FeedbackController < ApplicationController
  def new
    @feedback = Feedback.new
    if current_user
      @feedback.name = current_user.full_name
      @feedback.email = current_user.email
    end
  end

  def create
    @feedback = Feedback.new(params[:feedback])
    @feedback.request = request
    if @feedback.deliver
      redirect_to feedback_path, notice: 'Спасибо за обращение! Очень скоро мы Вам ответим'
    else
      flash.now[:error] = 'Не удалось отправить сообщение :('
      render :new
    end
  end
end
