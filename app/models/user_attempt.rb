class UserAttempt < ActiveRecord::Base
  validates :user, :disk, presence: true
  belongs_to :user
  belongs_to :disk
  has_many :user_answers, dependent: :destroy

  def to_s
    "#{user} attempt at #{disk} (##{id} at #{created_at}"
  end

  def self.get_current_by_user_disk(user, disk)
    UserAttempt.find_or_initialize_by({user: user, disk: disk, completed_at: nil})
  end

  def next_question_number(cur_question=0)
    unless disk.questions.empty?
      unanswered_indexes = questions.each_with_index.select {|q, i| !q[:answered]}.map {|pair| pair[1]}.sort
      (unanswered_indexes.select{|i| i + 1 > cur_question}.first || unanswered_indexes.first).to_i + 1
    end
  end

  def finish
    unless completed_at
      correct = user_answers.joins(:answer).where(answers: {correct: true}).count
      update_attributes({completed_at: DateTime.now, correct_count: correct})
    end
  end

  def questions
    answered_ids = user_answers.pluck(:question_id)
    disk.questions.map {|q| {question: q, answered: answered_ids.include?(q.id)} }
  end

  def remains_questions_count
    questions.count { |q| !q[:answered] }
  end

  def self.best_scores(user, disks)
    disks = [disks] unless disks.is_a? Array
    UserAttempt.where(disk_id: disks, user: user).group('disk_id').pluck(:disk_id, 'max(ifnull(correct_count, 0))').to_h
  end
end
