class UserAnswer < ActiveRecord::Base
  validates :user, :question, :answer, presence: true, allow_blank: false
  validates_each :answer_id do |record, attr, value|
    record.errors.add(attr, 'должен быть в числе ответов вопроса') if !record.question.answers.pluck(:id).include?(value) && !value.nil?
  end

  before_save :set_category
  before_save :set_correct
  after_save :update_user_question_result

  belongs_to :user
  belongs_to :question
  belongs_to :answer
  belongs_to :user_attempt

  def to_s
    "#{user} answer #{answer} to #{question} (#{user_attempt || 'no attempt'})"
  end

  private

  def set_correct
    self.correct = self.answer ? self.answer.correct : false
    true
  end

  def set_category
    self.category_id ||= self.question.category_id
    true
  end

  def update_user_question_result
    UserQuestionResult.find_or_create_by(user: user, question:question).update_attribute(:percentage, user_question_result)
  end

  def user_question_result
    percentage = 0
    question.user_answers.where(user: user).order('user_answers.updated_at, user_answers.id').each do |answer|
      if answer.correct
        percentage = [percentage + 25, 100].min
      else
        percentage = [percentage - 25, 0].max
      end
    end
    percentage
  end
end
