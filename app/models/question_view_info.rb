class QuestionViewInfo
  attr_accessor :question, :answer, :params, :options, :next_question
  attr_reader :category

  def initialize(question, params = {})
    self.question = question
    @answer = params[:answer]
    @params = params[:params] || {}
    @options = params[:options] || {}
  end

  def question=(question)
    @question = question
    @category = question.category
  end

  def answer_requested? answer
    @answer && @answer.id == answer.id
  end

  def answers
    @question.answers
  end

  def link_options
    { params: @params }.merge @options
  end

  def next_question
    if @next_question
      @next_question
    else
      current_index = ( @category.questions.index{ |q| q.id == @question.id } || -1 ) + 1
      current_index = 0 if current_index == @category.questions.count
      @category.questions[current_index]
    end
  end

  def find_next_question_within questions_ids
    current_index = ( questions_ids.index(@question.id) || -1 ) + 1
    current_index = 0 if current_index == questions_ids.count
    Question.find(questions_ids[current_index])
  end

  def find_next_question_within! questions
    @next_question = find_next_question_within questions
  end
end