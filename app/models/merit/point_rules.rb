# Be sure to restart your server when you modify this file.
#
# Points are a simple integer value which are given to "meritable" resources
# according to rules in +app/models/merit/point_rules.rb+. They are given on
# actions-triggered, either to the action user or to the method (or array of
# methods) defined in the +:to+ option.
#
# 'score' method may accept a block which evaluates to boolean
# (recieves the object as parameter)

module Merit
  class PointRules
    include Merit::PointRulesMethods

    def initialize
      # score 10, :on => 'users#update' do
      #   user.name.present?
      # end
      #
      # score 15, :on => 'reviews#create', :to => [:reviewer, :reviewed]
      #
      # score 20, :on => [
      #   'comments#create',
      #   'photos#create'
      # ]

      common_params = {
          model_name: 'UserAnswer',
          category: Merit::Points::MONEY,
          to: [:user]
      }

      score AppSettings.Points.category_started,
            common_params.merge({ on: %w(training/questions#answer training/disks#test_answer) }),
            &check_category_started_proc

      score AppSettings.Points.answers_correct,
            common_params.merge({ on: ['training/questions#answer'] }),
            &check_answers_correct_proc

      score -AppSettings.Points.answers_incorrect,
            common_params.merge({ on: ['training/questions#answer'] }),
            &check_answer_incorrect_proc
    end

    def check_category_started_proc
      Proc.new do |user_answer|
        same_answers(user_answer).count == 1
      end
    end

    def check_answers_correct_proc
      Proc.new do |user_answer|
        last_wrong_answer_date = same_answers(user_answer).where(correct: false).
            order(updated_at: :desc).pluck(:updated_at).first || Time.new(1)

        answered_questions = same_answers(user_answer).where('updated_at >= ?', last_wrong_answer_date).
            where(correct: true).
            pluck(:question_id, :id)

        same_day_answers = same_answers(user_answer).where(question_id: answered_questions.map(&:first)).
            where.not(id: answered_questions.map(&:last)).
            where(updated_at: Time.now.beginning_of_day..last_wrong_answer_date).
            pluck(:question_id).uniq

        answers_count = answered_questions.map(&:first).uniq.count - same_day_answers.count
        (answers_count > 0) && (answers_count % AppSettings.Points_restrictions.correct_answers_for_points == 0)
      end
    end

    def check_answer_incorrect_proc
      Proc.new do |user_answer|
        !user_answer.answer.correct?
      end
    end

    protected
    def same_answers(user_answer)
      UserAnswer.where(user_id: user_answer.user_id, category_id: user_answer.category_id)
    end
  end
end
