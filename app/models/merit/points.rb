module Merit
  class Points
    MONEY = 'money'

    def self.get_balance_for(user)
      get_points(MONEY, user)
    end

    def self.get_points(kind, user)
      # For newly registered users, if we use current_user and not read the user from the db, it creates a new sash
      # So just reload it
      user.reload
      user.points(category: kind)
    end
  end
end
