class Category < ActiveRecord::Base
  validates_presence_of :slug, :name

  # See http://norman.github.io/friendly_id/4.0/
  extend FriendlyId
  friendly_id :slug, use: [:finders]


  has_many :questions, dependent: :destroy
  has_many :disks, dependent: :destroy

  def next_disk(disk)
    disks.at(disks.index(disk) + 1)
  end
end
