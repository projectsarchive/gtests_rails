class UserQuestion < Question
  attr_accessor :percentage, :started, :last_answered
  attr_reader :user_id
  after_initialize :set_virtual_attributes

  default_scope { no_duplicates }

  def self.user(user)
    @user = user
    joins_sql = <<-SQL
      LEFT OUTER JOIN user_question_results
        ON questions.id=user_question_results.question_id
        AND user_question_results.user_id=#{@user.id}
    SQL
    select_sql = <<-SQL
    questions.*,
    ifnull(user_question_results.user_id, #{user.id}) user_id,
    ifnull(user_question_results.percentage, 0) percentage,
    (user_question_results.percentage IS NOT NULL) started,
    user_question_results.updated_at last_answered
    SQL
    self.joins(joins_sql).select(select_sql)
  end

  scope :next_for, -> (question) { where('questions.id > ?', question.id) }

  def percentage!
    result = user_question_results.where(user_id: @user_id).first
    @percentage = result ? result.percentage : 0
    @started = !@percentage.nil?
    @last_answered = result ? result.updated_at : nil
    @percentage
  end

  scope :user_learned, -> { where('ifnull(user_question_results.percentage, 0) >= 99') }
  scope :user_new, -> { where('user_question_results.percentage IS NULL') }
  scope :user_learning, -> { where('user_question_results.percentage IS NOT NULL and ifnull(user_question_results.percentage, 0) < 99') }

  private

  def set_virtual_attributes
    @percentage = attributes['percentage']
    @started = attributes['started'] == 1
    @user_id = attributes['user_id']
    @last_answered = attributes['last_answered']
  end
end
