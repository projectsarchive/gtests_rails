class Feedback < MailForm::Base
  attribute :name, validate: true
  attribute :email, validate: /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :subject
  attribute :message, validate: true
  attribute :nickname, captcha: true

  def headers
    {
        subject: subject.presence || 'Обращение с сайта 1c-test.ru',
        to: AppSettings.support_email,
        from: %("#{name}" <#{email}>),
        delivery_method: :sendmail
    }
  end
end
