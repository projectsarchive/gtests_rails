class User < ActiveRecord::Base
  has_merit

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :identities, dependent: :destroy
  has_many :user_attempts, dependent: :destroy
  has_many :user_answers, dependent: :destroy
  has_many :payments, dependent: :destroy

  include Names
  include Newable

  def valid_password?(password)
    super(password) || current_password_can_be_blank?
  end

  def email_required?
    super && identities.blank?
  end

  protected

  def current_password_can_be_blank?
    !identities.empty? && encrypted_password.blank?
  end
end
