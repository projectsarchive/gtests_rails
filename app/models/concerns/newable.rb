module Newable
  include ActiveSupport::Concern

  attr_accessor :is_new

  def is_new?
    @is_new || self.new_record?
  end
end
