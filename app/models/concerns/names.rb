module Names
  extend ActiveSupport::Concern

  def full_name
    name = (first_name.to_s + ' ' + last_name.to_s).strip
    return name unless name.blank?
    return nickname unless nickname.blank?
    return email unless email.blank?
    'Безымянный'
  end
end