class Answer < ActiveRecord::Base
  belongs_to :question
  has_many :user_answers, dependent: :destroy

  default_scope { order('answers.question_id, answers.order ASC, answers.id') }

  def to_s
    text.try(:truncate, 30)
  end
end
