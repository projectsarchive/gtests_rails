class Disk < ActiveRecord::Base
  belongs_to :category
  has_many :questions, counter_cache: true
  has_many :user_attempts, dependent: :destroy

  scope :with_questions, -> { joins(:questions).select('disks.*, count(questions.id) as q_count').group('disks.id') }

  def to_s
    name
  end

  def index_of question
    questions.index(question) + 1 if questions.include? question
  end

  def name_in_lists(params={})
    name.truncate(25, params.merge({separator: ' '}))
  end
end
