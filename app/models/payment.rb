class Payment < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :user
  before_create :count_points
  after_create :add_points

  private

  def count_points
    self.points = (self.withdraw_amount || 0) / (AppSettings.POINTS_RATE || 1)
  end

  def add_points
    self.user.add_points(self.points, category: Merit::Points::MONEY)
  end
end
