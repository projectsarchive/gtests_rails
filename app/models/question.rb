class Question < ActiveRecord::Base
  belongs_to :disk
  belongs_to :category
  has_many :answers, dependent: :destroy
  has_many :user_answers, dependent: :destroy
  has_many :user_question_results, dependent: :destroy

  has_many :duplicates, class_name: 'Question', foreign_key: 'duplicate_of_id'
  belongs_to :duplicate_of, class_name: 'Question'

  default_scope { order('questions.disk_id, questions.order ASC, questions.id') }
  scope :no_duplicates, -> { where(duplicate_of: nil) }

  def to_s
    text.truncate(50)
  end

  def correct_answer
    answers.where(correct: true).first
  end

end
