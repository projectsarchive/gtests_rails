onQuestionModalClose = ->
  window.location.hash = '_'

showQuestionModal = ->
  $('#question-modal').modal('show').on('hidden.bs.modal', onQuestionModalClose)

hashchange = ->
  if matches = window.location.hash.match(/\/(\d+)/)
    $.getScript("#{window.location.pathname}/#{matches[1]}.js").done(showQuestionModal).fail(onQuestionModalClose)

ready = ->
  filter_buttons = $('#questions-filter > button')
  questions_list = $('#questions-list')

  # Filters

  filter_buttons.click ->
    questions_list.html('<div class="icons-disqus-spinner"></div>')
    current_element = $(this)
    filter = $(this).data('filter')
    $.ajax "#{window.location.pathname}.js",
      data:
        filter: filter
      cache: false
      dataType: 'html'
      error: (jqXHR, textStatus, errorThrown) ->
        questions_list.html "AJAX Error: #{textStatus}"
      success: (data, textStatus, jqXHR) ->
        questions_list.html data
        setScrollListener()
        filter_buttons.removeClass 'active'
        current_element.addClass 'active'
        CountersInteraction.hit("#!/filter/#{filter}")
    false

  # Pagination

  $('#questions-list .pagination').remove()

  scrollFunction = ->
    filter = filter_buttons.filter('.active').data('filter') || 'all'
    next_page_div = questions_list.find('.next-page').last()
    if next_page_div.data('page') > 0 && $(window).scrollTop() > $(document).height() - $(window).height() - 50
      page = next_page_div.data('page')
      # Prevent firing new ajax call on each scroll
      next_page_div.data('page', '')
      $.ajax "#{window.location.pathname}.js",
        data:
          filter: filter
          page: page
        cache: false
        dataType: 'html'
        success: (data, textStatus, jqXHR) ->
          next_page_div.remove()
          questions_list.append data
          setScrollListener()
          CountersInteraction.hit("#!/filter/#{filter}/#{page}")

  setScrollListener = ->
    if questions_list.find('.next-page').last().length
      $(window).scroll(scrollFunction)
    else
      $(window).off("scroll", scrollFunction)

  setScrollListener()

  clearModal = ->
    $('#question-modal #question-header').html('')
    $('#question-modal .modal-body').html('<div class="icons-disqus-spinner"></div>')

  # Testing
  questionClickFunction = (e)->
    showQuestionModal()
    clearModal()

  handle_ajax_error = (response) ->
    try
      console.log(response)
      responseText = $.parseJSON(response.responseText)
    catch
      responseText = null
    if responseText
      responseMsg = responseText.error;
    else
      responseMsg = 'Произошла неизвестная ошибка или превышено время ожидания ответа от сервера. Пожалуйста, повторите попытку позже'
    responseMsg

  $('#questions-list').on('click', '.question-link', questionClickFunction)

  $('#question-modal').on 'ajax:before', 'form', ->
#    $('#submit-spinner, #submit-button').toggle()
    $('#submit-button').addClass('disabled')
  $('#question-modal').on 'ajax:error', 'form', (data, response) ->
    $('#question-error-explanation').html(handle_ajax_error(response))
    $('#submit-button').removeClass('disabled')

  hashchange()

$(document).ready(ready)
$(document).on('page:load', ready)
$(window).on('hashchange', hashchange)
