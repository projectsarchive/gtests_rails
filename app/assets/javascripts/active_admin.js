//= require jquery
//= require best_in_place
//= require best_in_place.purr
//= require rich

$(document).ready(function() {
    /* Activating Best In Place */
    jQuery(".best_in_place").best_in_place();
});
