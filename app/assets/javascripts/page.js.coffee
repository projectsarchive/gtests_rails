# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

page_ready = ->
  $('.faq > :not(h1):not(h2):not(h3)').hide()
  $('.faq > h3').click ->
    $(this).nextUntil('h1,h2,h3').toggle()

$(document).ready(page_ready)
$(document).on('page:load', page_ready)
