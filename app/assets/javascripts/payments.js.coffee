ready = ->

  $('#points-income #amount').prop('disabled', false)
  $('#points-income #amount').change ->
    value = parseInt($(this).val())
    if !value || value <= 0
      value = 1
    $(this).val(value) unless $(this).val() == value
    $('#points-income #sum').val(+(value * $('#points-income').data('rate')).toFixed(2))

  $('#payments-joyride').removeClass('hidden').append('<div id="payments-overlay"></div>')

  $('#payments-startride').click (e) ->
    e.preventDefault
    $('#overlay').show
    $(this).BootJoyride
      'cookieMonster': false
      'cookieName': '1ctestPaymentJoyride'
      'cookieDomain': false
      'tipContent': '#PaymentHints'
      'postRideCallback': payments_endride
      'postStepCallback': $.noop
      'nextOnClose': false
      'debug': false

payments_endride = ->
  $('#payments-overlay').hide

$(document).ready(ready)
$(document).on('page:load', ready)
