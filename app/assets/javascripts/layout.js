var ready = function() {
    $('body').removeClass('no-js');

    $.ajax({
        url: "/comments/recent",
        cache: false,
        success: function(html){
            if(html) {
                $("#recent-comments").show();
                $("#recent-comments > div").hide().html(html).slideDown();
            } else {
                $("#recent-comments").hide();
            }
        }
    });

    $('[data-toggle="tooltip"]').tooltip();
}

$(document).ready(ready);
$(document).on('page:load', ready);
