json.extract! @category, :id, :name, :description, :base_code, :full_description, :seo_title, :seo_description, :seo_keywords, :created_at, :updated_at
