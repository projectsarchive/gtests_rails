json.array!(@categories) do |category|
  json.extract! category, :id, :name, :description, :base_code, :full_description, :seo_title, :seo_description, :seo_keywords
  json.url category_url(category, format: :json)
end
