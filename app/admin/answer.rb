ActiveAdmin.register Answer do
  menu parent: I18n.t('activeadmin.menu.content'), priority: 4

  permit_params :text, :correct, :order
  %w(question_id text).each {|field| filter field}

  config.sort_order = 'question_id_asc'

  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  index do
    column :id
    column :question, sortable: 'question_id'
    column :order, sortable: false do |i|
      best_in_place i, :order, type: :input, path: [:admin, i]
    end

    column :text do |item|
      item.text.truncate(100)
    end
    column :correct, sortable: false
    actions defaults: true
  end
  
end
