ActiveAdmin.register Disk do
  menu parent: I18n.t('activeadmin.menu.content'), priority: 2

  actions :all, except: [:edit]
  %w(category_id name code).each {|field| filter field}

  index do
    column :id
    column :category, sortable: 'category_id'
    column :name, sortable: false
    column :code
    column :created_at
    column :updated_at

    actions defaults: true
  end

  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
