ActiveAdmin.register Question do
  menu parent: I18n.t('activeadmin.menu.content'), priority: 3

  permit_params :disk_id, :text, :order, :explanation, :duplicate_of
  %w(category_id disk_id text code).each {|field| filter field}

  index do
    column :id
    column :category, sortable: 'category_id'
    column :disk_id
    column :text, sortable: false do |q|
      q.text.truncate(100)
    end
    column :code
    column :order
    column :created_at
    column :updated_at

    actions defaults: true
  end

  form do |f|
    f.inputs 'Details' do
      f.input :text
      f.input :order
      f.input :explanation, as: :rich, config: {width: '76%', height: '400px', class: 'gloss-class'}
      f.input :disk_id
      f.input :duplicate_of
    end

    f.actions
  end

  show do |q|
    attributes_table do
      row :text
      row :order
      row :explanation
      row :disk
      row :category
      row :duplicate_of do |q|
        best_in_place q, :duplicate_of, type: :input, path: [:admin, q]
      end
    end
    panel 'Ответы' do
      table_for q.answers do
        column :order do |answer|
          best_in_place answer, :order, type: :input, path: [:admin, answer]
        end
        column :text
      end
    end
    active_admin_comments
  end
  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
