ActiveAdmin.register Category do
  menu parent: I18n.t('activeadmin.menu.content'), priority: 1

  permit_params :name, :slug, :description, :title, :full_description, :seo_title, :seo_description, :seo_keywords
  %w(id slug name code).each {|field| filter field}

  index do
    column :id
    column :slug
    column :name
    column :description, sortable: false do |c|
      (c.description || '').truncate(100)
    end
    column :code
    column :created_at
    column :updated_at

    actions defaults: true
  end
  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
