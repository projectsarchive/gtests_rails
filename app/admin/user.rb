ActiveAdmin.register User do
  scope :all, default: true
  %w(email first_name last_name nickname).each { |field| filter field }
  filter :admin, as: :select

  index do
    %w(id email first_name last_name nickname current_sign_in_at last_sign_in_at created_at).each do |field|
      column field
    end

    column(:admin) do |user|
      user.admin? ? status_tag( "yes", :ok ) : status_tag( "no" )
    end

    column :points_actions
    column :num_points
    column :minus_points
    # column :plus_points

    actions defaults: true
  end

  controller do
    def scoped_collection
      User.select(
            'users.*',
            'SUM(CASE WHEN merit_scores.id IS NULL THEN 0 ELSE 1 END) points_actions',
            'SUM(ifnull(merit_score_points.num_points, 0)) num_points',
            'SUM(CASE WHEN merit_score_points.num_points > 0 THEN 0 ELSE merit_score_points.num_points END) minus_points'
            # 'SUM(CASE WHEN merit_score_points.num_points < 0 THEN 0 ELSE merit_score_points.num_points END) plus_points'
          ).
          joins('LEFT OUTER JOIN merit_scores ON merit_scores.sash_id = users.sash_id').
          joins('LEFT OUTER JOIN merit_score_points ON merit_score_points.score_id = merit_scores.id').
          group('users.id')
    end
  end
  
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end
  
end
