Feature: Points system

  Background: System with users and tests
    Given I am an ordinary logged in user with provider "Twitter"
    And there is another user with 55 points
    And there is a category named "Category 1"
    And there are 2 disks with 10 questions each in the category "Category 1"
    And all my points dropped

  Scenario: Getting points for registration
    Given I log out
    When I register in the system
    Then I should be added 50 points

  Scenario: Getting points for omniauth registration
    Given I log out
    When I log in with provider "Facebook"
    Then I should be added 50 points

  Scenario: Not getting points for adding omniauth account to an existing user
    When I add provider "Facebook"
    Then I should not be added any points

  Scenario: Not getting points for ominauth login of an existing user
    Given I log out
    And all my points dropped
    When I log in with provider "Twitter"
    Then I should not be added any points

  Scenario: Getting points for starting new category
    When I correctly answer question 5 of category "Category 1"
    Then I should be added 50 points

  Scenario: Getting points for starting new category by disks
    When I answered 1 question of the 1 disk and left
    Then I should be added 50 points

  Scenario: Not getting points for submitting answer without any selection
    When I give no answer for question 3 of category "Category 1"
    Then I should not be added any points

  Scenario: Not getting points for continuing a category
    Given I answer question 5 of category "Category 1"
    And all my points dropped
    When I answer question 6 of category "Category 1"
    And I answer question 5 of category "Category 1"
    Then I should not be added any points

  Scenario: Getting points for correct answers
    Given I started category "Category 1"
    When I correctly answer 5 questions of category "Category 1"
    Then I should be added 5 points

  Scenario: Not getting points for correctly answering 6th question
    Given I started category "Category 1"
    And I correctly answer 5 questions of category "Category 1"
    And all my points dropped
    When I correctly answer question 6 of category "Category 1"
    Then I should not be added any points

  Scenario: Not getting points for correctly answering one question 6 times
    Given I started category "Category 1"
    And all my points dropped
    When I correctly answer question 6 of category "Category 1" 6 times
    Then I should not be added any points

  Scenario: Not getting points for recent answers
    When I incorrectly answer question 1 of category "Category 1"
    And I correctly answer 5 questions of category "Category 1"
    Then I should not be added 5 points

  Scenario: Not getting points for answering questions by disk
    Given I started category "Category 1"
    When I answered 5 questions of the 1 disk and left
    Then I should not be added any points

  Scenario: Not letting user to try a question with a zero points balance
    Given I have zero points balance
    When I visit question 1 training path
    Then I should not see question 1 text
    And I should not see question 1 answers
    And I should see payment window
    And I should see "необходим положительный баланс бонусов"

  Scenario: Letting user to answer questions by disk with a zero points balance
    Given I have zero points balance
    When I visit the 1 disk training path
    Then I should see question 1 text
    And I should see question 1 answers
    And I should see button "Ответить на вопрос"

  @javascript
  Scenario: Not letting user to try a question with zero points balance - ajax
    Given I have zero points balance
    When I visit the "Category 1" questions training path
    And I click title of the question 5
    And I should not see question 5 answers
    And I should see payment window
    And I should see "необходим положительный баланс бонусов"

  Scenario: Subtracting points for a wrong answer
    Given I started category "Category 1"
    And I have 10 points balance
    When I give an incorrect answer for question 1 of category "Category 1"
    Then I should be subtracted 1 point

  Scenario: Subtracting points for two wrong answers for the same question within same day
    Given I started category "Category 1"
    And I have 10 points balance
    When I give an incorrect answer for question 1 of category "Category 1"
    And I give an incorrect answer for question 1 of category "Category 1"
    Then I should be subtracted 2 points

  Scenario: Not touching points for wrong answers when testing by disks
    Given I started category "Category 1"
    And I have 10 points balance
    When I incorrectly answered 1 questions of the 1 disk and left
    Then I should not be subtracted any points

  @javascript
  Scenario: Updating points balance in the list when testing by questions (AJAX)
    Given I started category "Category 1"
    And I have 10 points balance
    When I visit the "Category 1" questions training path
    Then I should see 10 points balance in the main widget
    When I click title of the question 1
    And wait for AJAX request finished
    Then I should see 10 points balance within the question window
    When I choose an incorrect answer for the 1 question
    And I click "Ответить на вопрос"
    And wait for AJAX request finished
    Then I should see 9 points balance within the question window
    And I should see 9 points balance in the main widget

  @skip
  Scenario: Getting points for completing a category
    When I have learned 80% of the questions of category "Category 1"
    Then I should be added 200 points
