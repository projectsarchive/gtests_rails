And(/^there are (\d+) disks with (\d+) questions each in the category "([^"]*)"$/) do |disks_count, questions_count, category_name|
  category = Category.find_by(name: category_name)
  create_list(:disk_with_questions, disks_count.to_i, category: category, questions_count: questions_count.to_i )
end

Then(/^I should see the (\d+) question of the (\d+) disk$/) do |question_number, disk_number|
  disk = Disk.all[disk_number.to_i - 1]
  question = disk.questions[question_number.to_i - 1]

  expect(page).to have_text question.text
end

When(/^I visit the (\d+) disk training path$/) do |disk_number|
  disk = Disk.all[disk_number.to_i - 1]
  visit category_disk_path(disk.category, disk)
end

And(/^I choose an? (correct|incorrect) answer for the (\d+) question of the (\d+) disk$/) do |correct, question_number, disk_number|
  question = Disk.all[disk_number.to_i - 1].questions[question_number.to_i - 1]
  answer = question.answers.where(correct: correct == 'correct').sample
  choose answer.text
end

When(/^I visit the (\d+) category disks training path$/) do |category_number|
  visit category_disks_path(Category.all[category_number.to_i - 1])
end

And(/^I answer all questions of the (\d+) disk with (\d+) errors?$/) do |disk_number, incorrect_answers|
  disk = Disk.all[disk_number.to_i - 1]
  disk.questions.count.times do |n|
    step "I give a #{n < incorrect_answers.to_i ? 'in' : ''}correct answer for the #{n+1} question of the #{disk_number} disk"
  end
end


And(/^I give an? (correct|incorrect) answer for the (\d+) question of the (\d+) disk$/) do |correct, question_number, disk_number|
  step "I choose a #{correct} answer for the #{question_number} question of the #{disk_number} disk"
  step 'I click "Ответить на вопрос"'
end

Given(/^I ?(incorrectly)? answered (\d+) questions? of the (\d+) disk and left$/) do |incorrect, questions_count, disk_number|
  step "I visit the #{disk_number} disk training path"
  questions_count.to_i.times { |n| step "I give a #{'in' if incorrect}correct answer for the #{n+1} question of the #{disk_number} disk" }
  visit root_path
end


Given(/^I made several attempts for the (\d+) disk and scored:$/) do |disk_number, scores|
  disk = Disk.all[disk_number.to_i - 1]
  scores.rows.each do |row|
    step "I visit the #{disk_number} disk training path"
    step "I answer all questions of the #{disk_number} disk with #{disk.questions.count - row[0].to_i} errors"
  end
end

Then(/^I should see my results to be (\d+) of (\d+)$/) do |correct_count, total_count|
  expect(page).to have_text "#{correct_count} из #{total_count}"
end

When(/^I go to the (\d+) disk history$/) do |disk_number|
  disk = Disk.all[disk_number.to_i - 1]
  visit history_category_disk_path(disk.category, disk)
end

Then(/^I should see a "([^"]*)" badge for the (\d+) disk$/) do |badge_text, disk_number|
  expect(page).to have_text badge_text
end

And(/^I should see a complete badge for the (\d+) disk$/) do |disk_number|
  expect(page).to have_css '.glyphicon-ok'
end

And(/^click the (\d+) row of the attempts list$/) do |row_number|
  first(:link, 'Детально').click
end

Then(/^I should see answers to the (\d+) attempt of the (\d+) disk$/) do |attempt_number, disk_number|
  disk = Disk.all[disk_number.to_i - 1]
  attempt = disk.user_attempts[attempt_number.to_i - 1]
  attempt.user_answers.each do |ua|
    expect(page).to have_text ua.answer.text
  end
end

And(/^I should see table of attempts with scores:$/) do |attempts|
  # attempts is a table.hashes.keys # => [:score]
  attempts.rows.each do |attempt|
    expect(page).to have_text "#{attempt[0]} из"
  end
end

Then(/^I should see the (\d+) disk history page$/) do |disk_number|
  expect(page).to have_text 'Результаты тестирования'
end


And(/^I click title of the (\d+) disk$/) do |disk_number|
  click_on Disk.all[disk_number.to_i - 1].name
end