And(/^I visit ([^_]+_path)$/) do |path|
  visit send(path)
end

When(/^I click "([^"]*)"$/) do |link|
  click_on link
end

And(/^show me the page$/) do
  save_and_open_page
end

And(/^I should see button "([^"]*)"$/) do |button_name|
  expect(page).to have_button button_name
end
