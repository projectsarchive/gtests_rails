Given(/^there is a category named "([^"]*)"$/) do |category_name|
  @category = create :category, name: category_name
end
