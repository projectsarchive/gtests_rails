And(/^I am an ordinary logged in user(?: with provider "([^"]*)")?$/) do |provider|
  password = 'password'
  @user = create :user, password: password, password_confirmation: password
  visit new_user_session_path
  fill_in "Email", with: @user.email
  fill_in "Пароль", with: password
  click_button 'Войти'
  if provider
    step "I add provider \"#{provider}\""
  end
end

When(/^I register in the system$/) do
  visit new_user_registration_path
  email = 'test@example.com'
  password = 'password'
  fill_in "Email", with: email
  fill_in "Пароль", with: password
  fill_in "Подтверждение пароля", with: password
  click_button 'Зарегистрироваться'
  @user = User.find_by(email: email)
end

When(/^I log in with(?: a)? provider(?: "([^"]*)")?$/) do |provider|
  provider ||= 'twitter'
  visit "/users/auth/#{provider.downcase}"
  @user = Identity.find_by(uid: OmniAuth.config.mock_auth[provider.downcase.to_sym].uid).user
end

When(/^I add provider "([^"]*)"$/) do |provider|
  step "I log in with provider \"#{provider}\""
end

And(/^check user$/) do
  puts "CheckUser: user.id = #{@user.id}"
end
