And(/^there (?:are|is) (\d+) ?(duplicate)? questions? in the category "([^"]*)"$/) do |questions_count, duplicate, category_name|
  questions = create_list :question, questions_count.to_i, category: Category.find_by(name: category_name)
  if duplicate
    questions.take(questions.count - 1).each { |q| q.update({duplicate_of: questions.last}) }
  end
end

When(/^I ?(correctly|incorrectly)? answer question (\d+) of category "([^"]*)"(?: (\d+) times?)?$/) do |correctness, question_number, category_name, repeat|
  (repeat || 1).times do
    step %{I give a #{correctness == 'correctly' ? 'correct' : 'incorrect'} answer for question #{question_number} of category "#{category_name}"}
  end
end

And(/^I (correctly|incorrectly) answer (\d+) questions? of category "([^"]*)"$/) do |correctness, questions_count, category_name|
  (1..questions_count).each do |i|
    step %{I #{correctness} answer question #{i} of category "#{category_name}"}
  end
end

When(/^I give (?:an?)? ?(correct|incorrect|no) answer for question (\d+) of category "([^"]+)"$/) do |answer_type, question_number, category_name|
  @categories = Hash.new unless defined?(@categories)
  category = @categories[category_name] ||= Category.includes(questions: [:answers]).find_by!(name: category_name)
  question = category.questions.offset(question_number - 1).first

  visit category_question_path(category, question)
  if answer_type != 'no'
    answer = question.answers.where(correct: answer_type == 'correct').first
    choose answer.text
  end
  click_on 'Ответить на вопрос'
end

When(/^I visit question (\d+) training path$/) do |question_number|
  question = Question.offset(question_number - 1).limit(1).first
  visit category_question_path(question.category, question)
end

Then(/^I should ?(not)? see question (\d+) ?(?:of category "([^"]*)")? text$/) do |negative, question_number, category|
  questions = Question.all
  questions = questions.where(category: Category.where(name: category).first) if category
  question = questions.offset(question_number - 1).limit(1).first
  expect(page).send("to#{ '_not' if negative}", have_text(question.text))
end

And(/^I should ?(not)? see question (\d+) ?(?:of category "([^"]*)")? answers$/) do |negative, question_number, category|
  questions = Question.all
  questions = questions.where(category: Category.where(name: category).first) if category
  question = questions.offset(question_number - 1).limit(1).first
  question.answers.each do |a|
    expect(page).send("to#{ '_not' if negative}", have_text(a.text))
  end
end
