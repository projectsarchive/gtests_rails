CAPTURE_A_NUMBER = Transform /^(-?\d+)$/ do |number|
  number.to_i
end

CAPTURE_HASH = Transform /^(empty)? ?hash(?: "([^"]+)")?$/ do |empty, hash_string|
  hash = ''
  unless empty
    hash = hash_string.gsub(/##(\w+)\s*(\d+)\.(\w+)##/) { $1.capitalize.constantize.offset($2.to_i - 1).pluck($3.to_s).first }
    hash = hash[1..-1] if hash[0] == '#'
  end
  hash
end

And(/^I visit the "([^"]*)" questions training path(?: with (#{CAPTURE_HASH}))?$/) do |category_name, hash|
  visit root_path if hash # If we were on the same page w/o hash, it wouldn't refresh with visit
  visit category_questions_path(Category.find_by_name!(category_name), anchor: hash)
end

When(/^I click title of the question (\d+)$/) do |question_number|
  question = @category.questions[question_number - 1]
  click_on question.text
end


Then(/^I should see text of the question (\d+)(?: of category "([^"]*)")?$/) do |question_number, category|
  step %{I should see question #{question_number}#{" of category \"#{category}\"" if category} text}
end

And(/^I should see answers of the question (\d+)(?: of category "([^"]*)")?$/) do |question_number, category|
  step %{I should see question #{question_number}#{" of category \"#{category}\"" if category} answers}
end

And(/^I choose an? (correct|incorrect) answer for the (\d+) question$/) do |correct, question_number|
  correct = correct == 'correct'
  question = @category.questions[question_number-1]
  choose question.answers.where(correct: correct).first.text
end

Given(/^I gave (\d+) correct answers? to the (\d+) question$/) do |correct_answers, question_number|
  create_list :correct_user_answer, correct_answers, user: @user, question: @category.questions[question_number-1]
end

And(/^I(?: should)? see (\d+)% completion badge$/) do |completion_badge|
  expect(page).to have_css ".icons-completion-#{completion_badge}"
end

And(/^I should see the (correct|incorrect) answer$/) do |correct|
  expect(page).to have_css ".#{correct}-answer"
end

When(/^I interactively ((?:in)?correctly) answer question (\d+)$/) do |correct, question_number|
  correct = correct[0..-3]
  step %{I click title of the question #{question_number}}
  step %{I choose a #{correct} answer for the #{question_number} question}
  step %{I click "Ответить на вопрос"}
end

And(/^I filter questions by "([^"]*)"$/) do |filter_name|
  click_on filter_name
end

And(/^I close question modal$/) do
  first('button.close').click
end

And(/^I should see "([^"]*)"$/) do |text|
  expect(page).to have_text text
end


Then(/^I should see (\d+)% completion badge for question (\d+) in list$/) do |completion_badge, question_number|
  question = @category.questions[question_number-1]
  within("#q-#{question.id}") do
    expect(page).to have_selector ".icons-completion-#{completion_badge}"
  end
end

And(/^I should see question (\d+) last answer date$/) do |question_number|
  question = @category.questions[question_number-1]
  expect(page).to have_text question.user_answers.where(user: @user).first.updated_at.strftime('%d.%m.%Y')
end

# See http://artsy.github.io/blog/2012/02/03/reliably-testing-asynchronous-ui-w-slash-rspec-and-capybara/
# and http://www.elabs.se/blog/53-why-wait_until-was-removed-from-capybara
And(/^wait for AJAX requests? finished$/) do
  expect{page.evaluate_script('jQuery.active == 0')}.to become_true
end

Then(/^page should have (#{CAPTURE_HASH})$/) do |hash|
  step %{wait for AJAX requests finished}
  page_hash = evaluate_script ' window.location.hash'
  hash = "##{hash}" if hash.present?
  expect(page_hash).to eq hash
end

And(/^I open current page with (#{CAPTURE_HASH})$/) do |hash|
  step %{wait for AJAX requests finished}
  uri = URI.parse(current_url)
  new_page = "#{uri.path}##{hash}"
  visit new_page
  step %{wait for AJAX requests finished}
end
