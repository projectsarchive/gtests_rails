And(/^there is another user with (\d+) points$/) do |points_count|
  password = 'password'
  user = create :user, password: password, password_confirmation: password
  user.add_points(points_count, category: Merit::Points::MONEY)
end

Then(/^I should ?(not)? be added ((-?\d+)|any|some) points$/) do |negative, points, points_count|
  score_points = @user.score_points(category: Merit::Points::MONEY)
  last_points = score_points.order(id: :desc).sum(:num_points)
  last_points -= @base_balance
  if points.to_i != 0
    expect(last_points).send("to#{'_not' if negative}", eq(points_count))
  else
    expression = negative ? be <= 0 : be > 0
    expect(last_points).to expression
  end
end

Then(/^I should ?(not)? be subtracted ((\d+)|any) points?$/) do |negative, points, points_count|
  step %{I should #{negative} be added #{points_count ? -points_count : points} points}
end

And(/^all my points dropped$/) do
  step %{I have 3 points balance}
end

Given(/^I started category "([^"]+)"$/) do |category_name|
  step %{I correctly answer question 15 of category "#{category_name}"}
  step %{I incorrectly answer question 16 of category "#{category_name}"}
  step %{all my points dropped}
end

Given(/^I log out$/) do
  click_link 'Выйти'
  @base_balance = 0
end

Given(/^I have (zero|\d+) points balance$/) do |points|
  points = 0 if points == 'zero'
  @base_balance = points
  @user.subtract_points(@user.points(category: Merit::Points::MONEY) - @base_balance, category: Merit::Points::MONEY)
end

And(/^I should see payment window$/) do
  expect(page).to have_text 'Сумма к оплате'
  expect(page).to have_button 'Пополнить'
end

And(/^show my balance(?: for "([^"]+)")?$/) do |tag|
  puts "User balance: #{@user.points(category: Merit::Points::MONEY)}#{" (#{tag})" if tag}"
end

Then(/^I(?: should)? see (?:my current)|(?:(\d+) points?) balance ((?:within the question window)|(?:in the main widget))$/) do |points, placement|
  balance = points || @user.points(category: Merit::Points::MONEY)
  within_element = if placement =~ /main widget/
             '#personal-area'
           elsif placement =~ /question window/
             '.modal-dialog'
           else
             'body'
           end
  within(within_element) do
    expect(page).to have_text "#{balance} балл"
  end
end
