Feature: Testing by disks
  As a ordinary user I want to test myself with questions grouped in disks
  And see the history of my attempts

  Background: A fully functioning system with tests
    Given I am an ordinary logged in user
    And there is a category named "Управление торговлей"
    And there are 3 disks with 14 questions each in the category "Управление торговлей"
    And I visit root_path

  Scenario: Begin testing with clicking on a disk name
    When I click "Тесты 1С"
    And I click "По дискам"
    And I click title of the 1 disk
    Then I should see the 1 question of the 1 disk

  Scenario: Answer question
    When I visit the 1 disk training path
    And I give a correct answer for the 1 question of the 1 disk
    Then I should see the 2 question of the 1 disk

  Scenario: Complete disk training with fail
    When I visit the 1 disk training path
    And I answer all questions of the 1 disk with 4 errors
    Then I should see my results to be 10 of 14

  Scenario: See best attempt score in the disks list
    Given I made several attempts for the 1 disk and scored:
      |score|
      |8    |
      |11   |
    And I made several attempts for the 2 disk and scored:
      |score|
      |5    |
      |14   |
    When I visit the 1 category disks training path
    Then I should see a "80%" badge for the 1 disk
    And I should see a complete badge for the 2 disk

  Scenario: 99% badge
    Given I visit the 1 disk training path
    And I answer all questions of the 1 disk with 1 error
    And I visit the 1 category disks training path
    Then I should see a "99%" badge for the 1 disk

  Scenario: See attempts history
    Given I made several attempts for the 1 disk and scored:
      |score|
      |8    |
      |10   |
      |13   |
      |5    |
    When I go to the 1 disk history
    And I should see table of attempts with scores:
      |score|
      |8    |
      |10   |
      |5    |

  Scenario: See detailed info about an attempt
    Given I made several attempts for the 1 disk and scored:
      |score|
      |10   |
    When I go to the 1 disk history
    And click the 1 row of the attempts list
    Then I should see answers to the 1 attempt of the 1 disk

#  @selenium
  Scenario: Continue interrupted attempt
    Given I answered 5 questions of the 1 disk and left
    When I visit the 1 disk training path
    Then I should see the 6 question of the 1 disk

  Scenario: See disk history from disks list
    Given I made several attempts for the 1 disk and scored:
      |score|
      |5    |
      |7    |
    And I visit the 1 category disks training path
    And I click "50%"
    Then I should see the 1 disk history page
