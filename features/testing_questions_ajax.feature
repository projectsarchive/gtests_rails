@javascript
Feature: Testing by questions with Ajax

  Background: Category with questions
    Given there is a category named "Category 1"
    And there are 10 questions in the category "Category 1"
    And I am an ordinary logged in user
    And I have 100 points balance
    And I visit the "Category 1" questions training path

  Scenario: Open question form
    When I click title of the question 1
    Then I should see text of the question 1
    And I should see answers of the question 1

  Scenario: Answer question
    Given I gave 2 correct answers to the 1 question
    When I click title of the question 1
    And I see 50% completion badge
    And I choose an incorrect answer for the 1 question
    And I click "Ответить на вопрос"
    Then I should see text of the question 1
    And I should see answers of the question 1
    And I should see 25% completion badge
    And I should see the correct answer
    And I should see the incorrect answer

  Scenario: Get next question without filter
    When I interactively correctly answer question 1
    And I click "Следующий вопрос"
    Then I should see text of the question 2
    And I should see answers of the question 2

  Scenario: Get next question with filter
    Given I gave 2 correct answers to the 1 question
    And I gave 1 correct answer to the 3 question
    And I filter questions by "На изучении"
    When I interactively incorrectly answer question 1
    And I click "Следующий вопрос"
    Then I should see text of the question 3

  Scenario: Get correct completion icon after first correct answer
    When I interactively correctly answer question 1
    Then I should see 25% completion badge

  Scenario: Updating progress and last answer of questions in list
    When I interactively correctly answer question 1
    And I close question modal
    Then I should see 25% completion badge for question 1 in list
    And I should see question 1 last answer date

  Scenario: Setting hash while testing
    Then page should have empty hash
    When I click title of the question 1
    Then page should have hash "#!/##question1.id##"
    When I choose a correct answer for the 1 question
    And I click "Ответить на вопрос"
    Then page should have hash "#!/##question1.id##"
    When I click "Следующий вопрос"
    Then page should have hash "#!/##question2.id##"

  Scenario: Open question on same page with change of the hash
    When I click title of the question 1
    And I open current page with hash "#!/##question2.id##"
    Then I should see question 2 answers

  Scenario: Open question by hash
    When I visit the "Category 1" questions training path with hash "#!/##question1.id##"
    Then I should see question 1 answers

  Scenario: Not displaying duplicate questions
    Given there is a category named "Category 2"
    And there are 2 duplicate questions in the category "Category 2"
    When I visit the "Category 2" questions training path
    Then I should not see question 1 of category "Category 2" text
    And I should see question 2 of category "Category 2" text
