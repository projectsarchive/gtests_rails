class AddDuplicateOfToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :duplicate_of_id, :integer
    add_index :questions, :duplicate_of_id
  end
end
