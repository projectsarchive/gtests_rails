class CreateDisks < ActiveRecord::Migration
  def change
    create_table :disks do |t|
      t.string :name
      t.string :code
      t.references :category, index: true

      t.timestamps
    end
  end
end
