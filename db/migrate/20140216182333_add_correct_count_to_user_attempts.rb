class AddCorrectCountToUserAttempts < ActiveRecord::Migration
  def change
    add_column :user_attempts, :correct_count, :integer
  end
end
