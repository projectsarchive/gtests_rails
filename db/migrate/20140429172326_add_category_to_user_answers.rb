class AddCategoryToUserAnswers < ActiveRecord::Migration
  def change
    add_reference :user_answers, :category, index: true
  end
end
