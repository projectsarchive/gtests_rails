class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.references :question, index: true
      t.text :text
      t.boolean :correct
      t.string :code
      t.integer :order

      t.timestamps
    end
  end
end
