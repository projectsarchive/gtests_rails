class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :notification_type
      t.string :operation_id
      t.float :amount
      t.float :withdraw_amount
      t.datetime :datetime
      t.string :sender
      t.boolean :codepro
      t.string :label
      t.references :user, index: true
      t.float :points

      t.timestamps
    end
  end
end
