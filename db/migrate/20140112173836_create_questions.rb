class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :text
      t.string :code
      t.string :image
      t.string :code1c
      t.integer :order
      t.references :disk, index: true
      t.references :category, index: true

      t.timestamps
    end
  end
end
