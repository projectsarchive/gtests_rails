class CreateUserQuestionResults < ActiveRecord::Migration
  def change
    create_table :user_question_results do |t|
      t.references :question, index: true
      t.references :user, index: true
      t.float :percentage

      t.timestamps
    end
  end
end
