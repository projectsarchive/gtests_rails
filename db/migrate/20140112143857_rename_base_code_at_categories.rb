class RenameBaseCodeAtCategories < ActiveRecord::Migration
  def change
    rename_column :categories, :base_code, :code
  end
end
