class CreateUserAttempts < ActiveRecord::Migration
  def change
    create_table :user_attempts do |t|
      t.references :user, index: true
      t.references :disk, index: true
      t.datetime :completed_at

      t.timestamps
    end
  end
end
