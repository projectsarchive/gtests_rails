class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.text :description
      t.string :base_code
      t.text :full_description
      t.string :seo_title
      t.text :seo_description
      t.string :seo_keywords

      t.timestamps
    end
  end
end
