source 'https://rubygems.org'

ruby '2.1.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
# gem 'rails', '~> 4.1.0'
# See https://github.com/rails/rails/issues/13648
gem 'rails', github: 'rails/rails', ref: '968c581ea34b5236af14805e6a77913b1cb36238', branch: '4-1-stable'

# Use mysql as the database for Active Record
gem 'mysql2'
#gem 'pg'

# Use jquery as the JavaScript library
gem 'jquery-rails'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
gem 'turbolinks-redirect'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

gem 'devise'
gem 'omniauth-twitter'
gem 'omniauth-facebook'
gem 'omniauth-vkontakte'
gem 'omniauth-google-oauth2'
gem 'omniauth-yandex'

gem 'rails_config', github: 'ssidelnikov/rails_config', branch: 'feature/heroku_compatibility'
gem 'rails-i18n', '~> 4.0.0'

gem 'merit'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller', :platforms=>[:mri_19, :mri_20, :mri_21, :rbx]
  gem 'quiet_assets'
  gem 'rails_best_practices'
  gem 'spring'
  gem 'active_record_query_trace', require: false
  # See http://railscasts.com/episodes/372-bullet?view=asciicast
  gem 'bullet'
end

group :development, :test do
  gem 'rspec-rails', '~> 2.0'
  gem 'capybara'
  gem 'cucumber-rails', :require => false
  gem 'factory_girl_rails', '~> 4.0'
  gem 'selenium-webdriver'
  gem 'database_cleaner'
  gem 'guard-rspec'
  gem 'guard-cucumber'
  gem 'guard-livereload'
  gem 'guard-spring'
  gem 'rb-fchange', :require=>false
  gem 'rb-fsevent', :require=>false
  gem 'rb-inotify', :require=>false
  gem 'jasmine-rails'
  gem 'poltergeist'
end

group :assets do
  gem 'compass-rails'
  gem 'bootstrap-sass', '~> 3.1.1'
  gem 'sass-rails', '~> 4.0.3'
  gem 'sass'
# See http://stackoverflow.com/questions/16877028/why-does-compass-watch-say-it-cannot-load-sass-script-node-loaderror
  gem 'compass', '0.12.2'
# Use CoffeeScript for .js.coffee assets and views
  gem 'coffee-rails', '~> 4.0.0'
# Use Uglifier as compressor for JavaScript assets
  gem 'uglifier', '>= 1.3.0'
end

gem 'simple-navigation-bootstrap'
gem 'breadcrumbs_on_rails'
gem 'reformal_turbolinks'
gem 'nprogress-rails'
gem 'slim'
gem 'slim-rails'
gem 'bluecloth'
gem 'rdiscount'

gem 'activeadmin', github: 'gregbell/active_admin'
gem 'best_in_place', github: 'bernat/best_in_place'

gem 'rich', github: 'bastiaanterhorst/rich'
gem 'paperclip'
gem 'aws-sdk'
#gem 'active_admin_editor'

gem 'friendly_id'
gem 'newrelic_rpm'

gem 'nokogiri'

#gem 'simple_form'
gem 'formtastic-bootstrap'

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# Use unicorn as the app server
platforms :ruby do
  # linux
  gem 'unicorn'
end

# Use Capistrano for deployment
gem 'capistrano', '~> 2.15.5'

gem 'mail_form'

group :production do
  # Heroku plugin
  gem 'rails_12factor'
end

group :test do
  gem 'zeus'
  gem 'launchy'
end

# Use debugger
# gem 'debugger', group: [:development, :test]

gem 'dumper'
