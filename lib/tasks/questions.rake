namespace :questions do
  desc 'Find duplicate questions (with same text field) and set their duplicate_of field to the later one'
  task find_duplicates: :environment do
    duplicate_texts = duplicate_question_texts
    puts "Found #{duplicate_texts.count} duplicates"
    text = ''
    duplicates = []
    Question.unscoped.where(text: duplicate_texts).order(:text, code1c: :desc).each do |q|
      if q.text.strip.mb_chars.downcase != text
        text = q.text.strip.mb_chars.downcase
        update_duplicate_ids(duplicates)
        duplicates = []
      end
      duplicates << q
    end
    update_duplicate_ids(duplicates)
    puts "Finished. Remaining duplicate questions: #{duplicate_question_texts.count}"
  end

  def duplicate_question_texts
    Question.unscoped.select('text, count(id)').where(duplicate_of: nil).group(:text).having('count(id) > 1').pluck(:text)
  end

  def update_duplicate_ids(duplicates)
    return unless duplicates
    duplicates.each { |d| origin = d.duplicate_of if d.duplicate_of }
    origin ||= duplicates.first
    return unless origin
    updated = false
    duplicates.each do |d|
      if d != origin && !d.duplicate_of
        d.update({ duplicate_of: origin })
        updated = true
      end
    end
    puts "Duplicates found and fixed for '#{origin.text}' (id: #{origin.id})"
  end

end
