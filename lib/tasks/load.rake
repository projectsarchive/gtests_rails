namespace :load do
  desc 'Load tests from XML file'
  task :gtests_from => :environment do
    filename = task_params.first
    abort('No filename to load tests from is specified') if filename.blank?
    file = File.read(filename)
    @xml = Nokogiri::XML(file)

    @update_existing = task_params.include?('with_update')

    update_records Category do |category, node|
      category.slug = category.code
    end

    update_records Disk do |disk, node|
      update_parent(Category, disk, node)
    end

    update_records Question do |question, node|
      disk = update_parent(Disk, question, node)
      question.category_id = disk.category.id if disk
      disk
    end
    update_records Answer, {replace_columns: {name: 'text'}} do |answer, node|
      update_parent(Question, answer, node)
      answer.order = answer.code[-2,2].to_i
    end
  end

  desc 'Load explanations from the old site (Yii)'
  task :explanations_from => :environment do
    require 'csv'
    filename = task_params.first
    text = File.read(filename)
    csv = CSV.parse(text, headers: true, encoding: Encoding::UTF_8, col_sep: ';', quote_char: '"')
    csv.each do |row|
      q = Question.find_by(code: row['base_code'])
      unless q
        puts "Question with code '#{row['base_code']}' not found"
        next
      end

      if q.code1c != row['internal_code']
        puts "Question with code '#{row['base_code']}' has different internal code: db='#{q.code1c}', file='#{row['internal_code']}'"
        unless q.explanation.blank?
          puts 'Explanation not empty, skipping'
          next
        end
      end

      result = q.update_attribute(:explanation, row['explanation'])
      if result
        puts "Explanation for question with code '#{q.code}' successfully updated"
      else
        puts "There were errors updating question with code '#{q.code}': #{q.errors.full_messages.to_sentence}"
      end
    end
  end

  def update_parent(parent_class, record, node, code_name='code')
    code = node.xpath("../../#{code_name}").text
    parent = parent_class.find_by(code: code)
    record["#{parent_class.name.downcase}_id".to_sym] = parent.id if parent
    parent
  end

  def update_records(type, options = {})
    nodes = @xml.xpath("//#{type.name.downcase}")
    compare_key = (options[:compare_key] || 'code').to_s
    default_find_proc = Proc.new do |attrs|
      type.find_or_initialize_by(compare_key => attrs[compare_key])
    end
    replace_columns = options[:replace_columns]
    replace_columns = Hash[replace_columns.map{|k, v| [k.to_s, v.to_s]}] if replace_columns
    model_columns = type.column_names

    count = saved = updated = 0
    total_count = nodes.count
    nodes.each do |node|
      attributes = node.xpath('./*').map {|n| {n.name => n.text} if n.children.count == 1 }
      attributes = attributes.compact.reduce Hash.new, :merge
      if replace_columns
        attributes.delete_if { |k,v| replace_columns.has_value? k }
        attributes = Hash[attributes.map {|k, v| replace_columns[k] ? [replace_columns[k], v] : [k, v]}]
      end
      attributes.keep_if {|key, value| model_columns.include? key }

      unless options[:find_proc]
        throw Exception.new("Record or model does not have column '#{compare_key}'") unless attributes.keys.include? compare_key
      end
      record = (options[:find_proc] || default_find_proc).call(attributes, node)
      record.assign_attributes(attributes)
      if !block_given? || yield(record, node)
        new_record = record.new_record?
        if @update_existing || new_record
          if record.save
            saved += 1
            updated += 1 unless new_record
          end
        end
      end
      count += 1
      STDOUT.write "\rUpdating #{type.name.downcase}: #{count} of #{total_count}"
    end

    puts
    puts ({type: type.name, records: count, saved: saved, updated: updated})
    puts
  end

  def task_params
    if ARGV[0] =~ /\[(.*)\]/
      args = ARGV[0].match(/\[(.*)\]/)[1].split
    else
      args = ARGV
      args.shift
    end
    args.each { |param| task param.to_sym do ; end}
    args
  end

end
