module Kaminari
  module Helpers
    class Tag
      def page_url_for(page)
        page = ( page <= 1 ? nil : page ) unless @options[:first_digit]
        @template.url_for @params.merge(@param_name => page)
      end
    end
  end
end
