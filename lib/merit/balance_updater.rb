module Merit
  module BalanceUpdater
    def self.included(base)
      base.after_filter do |controller|
        if current_user && !controller.no_merit_update?
          money = Merit::Points::get_balance_for(current_user)
          new_balance = ActiveSupport::NumberHelper.number_to_currency(money, precision: 0, unit: '') + downcaseru(t(:internal_money, count: money))
          response.body = response.body.gsub '-1 балл', new_balance
        end
      end
    end

    protected

    def downcaseru(string)
      string.mb_chars.downcase.to_s
    end
  end
end
