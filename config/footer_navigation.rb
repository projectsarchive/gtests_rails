# -*- coding: utf-8 -*-
# Configures your navigation
SimpleNavigation::Configuration.run do |navigation|
  navigation.renderer = Renderer::Bootstrap

  navigation.items do |footer|
    footer.item :faq, 'FAQ', page_faq_path
    footer.item :feedback, 'Напишите нам', feedback_path
  end

end
