if Rails.env.development?
  require 'active_record_query_trace'
  ActiveRecordQueryTrace.enabled = true
  # ActiveRecordQueryTrace.level = :full
  # ActiveRecordQueryTrace.lines = 50
end
