RailsConfig.setup do |config|
  config.const_name = 'AppSettings'
  config.use_env = true
end