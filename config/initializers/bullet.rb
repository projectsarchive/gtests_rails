# See http://railscasts.com/episodes/372-bullet?view=asciicast
if defined? Bullet
  Bullet.enable = true
  Bullet.alert = true

  # UserAnswer.answer is updated in Training::Questions.answer
  Bullet.add_whitelist :type => :n_plus_one_query, :class_name => 'UserAnswer', :association => :answer
end
