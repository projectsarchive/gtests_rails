# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Gtests::Application.initialize!

Paperclip::Attachment.default_options.merge!(
    :storage => :s3,
    :bucket => AppSettings['S3_BUCKET'],
    :url => "/system/:class/:attachment/:id/:style/:filename",
    :s3_host_name => AppSettings['S3_HOST_NAME'],
    :s3_credentials => {
        :access_key_id => AppSettings['S3_ACCESS_KEY_ID'],
        :secret_access_key => AppSettings['S3_SECRET_ACCESS_KEY']
    }
)
