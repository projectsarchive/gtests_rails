require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
#Bundler.require(:default, Rails.env)
# Reason for the change - http://stackoverflow.com/questions/6005361/sass-import-error-in-rails-3-app-file-to-import-not-found-or-unreadable-comp
Bundler.require(*Rails.groups(:assets => %w(development test))) if defined?(Bundler)

module Gtests
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]

    # Turn off until https://github.com/rails/rails/issues/13164 is fixed
    config.i18n.enforce_available_locales = I18n.enforce_available_locales = false

    config.before_configuration do
      I18n.load_path += Dir[Rails.root.join('config', 'locales', '*.{rb,yml}').to_s]
      I18n.locale = :fr
      I18n.default_locale = :fr
      config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '*.{rb,yml}').to_s]
      config.i18n.locale = :fr
      # bypasses rails bug with i18n in production\
      I18n.reload!
      config.i18n.reload!
    end

    config.i18n.available_locales = :ru
    config.i18n.default_locale = :ru
    I18n.default_locale = I18n.locale = config.i18n.locale = config.i18n.default_locale


    # Enable the asset pipeline
    config.assets.enabled = true

    config.autoload_paths += Dir["#{config.root}/lib/"]

    config.action_mailer.delivery_method = :smtp

    # Fixing an issue with rails_config - https://github.com/railsjedi/rails_config/issues/59
    # It's also important to enable access to ENV while precompile - `heroku labs:enable user-env-compile -a gtests`
    initializer :set_cache_store, after: :load_rails_config_settings, before: :load_environment_config, group: :all do |app|
      config.action_mailer.smtp_settings = {
          address: 'smtp.yandex.ru',
          port: 587,
          domain: '1c-test.ru',
          authentication: :login,
          enable_starttls_auto: true,
          user_name: AppSettings.mail_login,
          password: AppSettings.mail_password
      }
    end
  end
end
