require 'bundler/capistrano'

default_run_options[:pty] = true
set :default_environment, {
    "PATH" => "/opt/rbenv/shims:/opt/rbenv/bin:$PATH"
}
set :ssh_options, { :forward_agent => true }

set :application, "gtests_production"
set :repository, "git@bitbucket.org:ssidelnikov/gtests_rails.git"
set :user, "deploy"
set :use_sudo, false
set :host, "app.1c-test.ru"

server host, :web, :app, :db, :primary => true

after "deploy:finalize_update", "symlink:all"

namespace :symlink do
  task :db do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/config/settings.local.yml #{release_path}/config/settings.local.yml"
  end
  task :all do
    symlink.db
  end
end

namespace :deploy do

  task :start do
    run "#{current_path}/bin/unicorn -Dc #{shared_path}/config/unicorn.rb -E #{rails_env} #{current_path}/config.ru"
  end

  task :restart do
    run "kill -USR2 $(cat #{shared_path}/pids/unicorn.pid)"
  end

  namespace :assets do
    # See http://stackoverflow.com/questions/9016002/speed-up-assetsprecompile-with-rails-3-1-3-2-capistrano-deployment

    desc 'Run the precompile only if assets changed'
    task :precompile, :roles => :web, :except => { :no_release => true } do
      from = source.next_revision(current_revision)
      if capture("cd #{latest_release} && #{source.local.log(from)} vendor/assets/ app/assets/ | wc -l").to_i > 0
        run %Q{cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} #{asset_env} assets:precompile}
      else
        logger.info "Skipping asset pre-compilation because there were no asset changes"
      end
    end

    # desc 'Run the precompile task locally and rsync with shared'
    # task :precompile, :roles => :web, :except => { :no_release => true } do
    #   from = source.next_revision(current_revision)
    #   if releases.length <= 1 || capture("cd #{latest_release} && #{source.local.log(from)} vendor/assets/ app/assets/ | wc -l").to_i > 0
    #     %x{bundle exec rake assets:precompile}
    #     %x{rsync --recursive --times --rsh=ssh --compress --human-readable --progress public/assets #{user}@#{host}:#{shared_path}}
    #     %x{bundle exec rake assets:clean}
    #   else
    #     logger.info 'Skipping asset pre-compilation because there were no asset changes'
    #   end
    # end

  end

end

after "deploy:restart", "deploy:cleanup"