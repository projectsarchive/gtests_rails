# GTests Platform

## Requirements

Ruby >= 2.0.0

## After clone

* database.default.yml -> database.yml. Correct the values.
* settings.local.default.yml -> settings.loca.yml. Fill in the necessary values, remove unused ones.
* bundle install
* rake db:setup

## Test suite

RSpec is used for tests. To run tests:

    $ spec ./spec

PhantomJS is used as a javascript driver for running feature specs. So it should be installed in the system.
See https://github.com/jonleighton/poltergeist for more directions. To install it on Mac OS systems it could be installed
this way:

    $ brew install phantomjs

### Guard

Guard CI is used in the project. It's advised to start guard before making any changes to the code and especially
before committing. To start guard type:

    $ bundle exec guard

### Database cleaner in tests

To use database_cleaner gem in the tests `require 'spec_helper_dbcleaner'` in the spec file. If you `require 'spec_helper'`
standard transactions will be used.

## Loading tests from XML

To load tests from XML after exporting from 1C use the following rake task:

    $ rake load:gtests_from filename.csv

The task would insert new records to the database. To update existing records use `with_update` param:

    $ rake load:gtests_from filename.csv with_update

## Application notifications

* `question.user_answered {:user_answer_id}` - invoked when a user answers the question or changed his answer.
* `users.create {:user_id}` - when a new user created (registered via a standard method or via a provider).

## Rake tasks

* `questions:find_duplicates` - searches for questions with same names and sets duplicate_of value to the last question in the list.

## Application configs

### settings.yml

* `redirect_domains` - a list of subdomains (eg [www, app]) to redirect to main domain (without that word)

