require "spec_helper"

describe Training::DisksController do
  describe "routing" do

    let!(:category) { FactoryGirl.create :category_with_disks }

    it "/category_slug/disks routes to #index" do
      get("/category_slug/disks").should route_to("training/disks#index", category_id: "category_slug")
    end

    it "/category_slug/disks/12 routes to #show" do
      get("/category_slug/disks/12").should route_to("training/disks#show", category_id: "category_slug", id: "12")
    end

    it "category_disk_path routes to #show" do
      get(category_disk_path(category, category.disks.first)).should route_to(
                                                                         "training/disks#show",
                                                                         category_id: category.slug.to_s,
                                                                         id: category.disks.first.id.to_s
                                                                     )
    end

    it "/category_slug/disks/12/7 routes to #test" do
      get('/category_slug/disks/12/7').should route_to('training/disks#test',
                                                       category_id: 'category_slug',
                                                       id: '12',
                                                       question_number: '7'
                                              )
    end

    it '/category_slug/disks/12/7/view routes to #view' do
      get('/category_slug/disks/12/7/view').should route_to('training/disks#view',
                                                       category_id: 'category_slug',
                                                       id: '12',
                                                       question_number: '7'
                                              )
    end

    it 'post /category_slug/disks/12/7 routes to #test_answer' do
      post('/category_slug/disks/12/7').should route_to('training/disks#test_answer',
                                                        category_id: 'category_slug',
                                                        id: '12',
                                                        question_number: '7'
                                               )
    end

    it 'get /slug/disks/12/finish routes to #finish' do
      get('/slug/disks/12/finish').should route_to('training/disks#finish',
                                                   category_id: 'slug',
                                                   id: '12'
                                          )
    end

  end
end
