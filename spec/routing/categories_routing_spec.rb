require "spec_helper"

describe CategoriesController do
  describe "routing" do

    it "/categories routes to #index" do
      get("/categories").should route_to("categories#index")
    end

    it "/category_slug routes to #show" do
      get("/category_slug").should route_to("categories#show", id: "category_slug")
    end

    it "/categories/id is not routable" do
      expect(get: "/categories/id").not_to be_routable
    end

  end
end
