require 'spec_helper'

describe UserAttempt do
  let(:attempt) { create :user_attempt }

  describe '.next_question_number' do
    subject { attempt.next_question_number }

    context 'when no answers given' do
      it { should eq 1 }
    end

    context 'when some answers given' do
      before { create :user_answer, user_attempt: attempt, question: attempt.disk.questions.first }

      it('returns next question number') { should eq 2 }
    end

    context 'when disk has no questions' do
      let(:attempt) { create :user_attempt, disk: create(:disk) }

      it { should be_nil }
    end

    context 'when all questions answered' do
      before { attempt.disk.questions.each { |q| create :user_answer, user_attempt: attempt, question: q } }

      it { should eq 1 }
    end

    context 'when cur_question passed' do
      context 'when there are unanswered questions before' do
        context 'and unanswered questions after' do
          it 'returns next unanswered question' do
            create :user_answer, user_attempt: attempt, question: attempt.disk.questions[2]
            expect(attempt.next_question_number(2)).to eq 4
          end
        end

        context 'and no unanswered questions after' do
          before { create :user_answer, user_attempt: attempt, question: attempt.disk.questions.last }

          it 'returns first unanswered from the beginning' do
            size = attempt.disk.questions.size
            expect(attempt.next_question_number(size - 1)). to eq 1
          end
        end
      end
    end
  end

  describe '.finish' do
    subject {attempt.finish}
    it 'updates completed_at date' do
      subject
      expect(attempt.completed_at).to_not be_nil
    end

    context 'for finished attempt' do
      let(:attempt) { create :user_attempt, completed_at: Time.now }
      it 'does not change completion date' do
        expect{subject}.to_not change{attempt.completed_at}
      end
    end
  end

  describe '.best_scores' do
    let(:user) { create :user }
    let(:disk) { create :disk }

    context 'with an attempt' do
      let!(:user_attempt) { create :user_attempt_with_answers, user: user, disk: disk, answers_count: 5, correct_answers: 3 }

      it 'returns best result' do
        expect(UserAttempt.best_scores(user, [disk])[disk.id]).to eq 3
      end

      context 'when several attempts' do
        let!(:user_attempt2) { create :user_attempt_with_answers, user: user, disk: disk, answers_count: 5, correct_answers: 1 }
        it 'returns best result' do
          expect(UserAttempt.best_scores(user, [disk])[disk.id]).to eq 3
        end
      end

      context 'when several disks provided' do
        let(:disk2) { create :disk }
        let!(:user_attempt2) { create :user_attempt_with_answers, user: user, disk: disk2, answers_count: 5, correct_answers: 2  }

        it 'returns a hash' do
          result = UserAttempt.best_scores(user, [disk, disk2])

          expect(result).to be_kind_of Hash
          expect(result[disk.id]).to eq 3
          expect(result[disk2.id]).to eq 2
        end
      end
    end

    context 'with no attempts' do
      it 'returns nil for count' do
        expect(UserAttempt.best_scores(user, [disk])[disk.id]).to be_nil
      end
    end
  end
end
