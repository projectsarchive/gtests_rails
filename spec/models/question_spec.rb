require 'spec_helper'

describe Question do
  subject(:question) { create :question }

  describe '.correct_answer' do
    subject { question.correct_answer }
    it { expect(question.answers.first.correct).to be_true }
    it { expect(subject).to eq question.answers.first }
    it { expect(subject) }
  end

  describe 'default scope' do
    subject { Question.all }
    context 'should consider questions order' do
      let!(:question1) { create :question, order: 2, disk: nil }
      let!(:question2) { create :question, order: 1, disk: nil }

      its('first.id') { should eq question2.id }
      its('last.id') { should eq question1.id }
      its(:count) { should eq 2 }
    end
  end
end
