require 'spec_helper'

describe Identity do
  it_behaves_like 'names'

  it 'returns correct image for twitter' do
    identity = Identity.new
    identity.provider = 'twitter'
    identity.image = 'http://twitter.com/image_0_normal.png'

    identity.image.should eq 'http://twitter.com/image_0.png'
  end
end
