require 'spec_helper'

describe UserAnswer do
  describe 'validation' do
    it 'should have answer of the question' do
      build(:user_answer, answer: create(:answer)).should_not be_valid
      build(:user_answer).should be_valid
    end
  end

  describe 'setting "correct" attribute' do
    let(:question) { create :question }
    context 'when answer correct' do
      let!(:answer) { create :answer, question: question, correct: true }
      it('to true') { expect(create(:user_answer, answer: answer, question: question).correct).to be_true }
    end

    context 'when answer incorrect' do
      let!(:answer) { create :answer, question: question, correct: false }
      it('to false') { expect(create(:user_answer, answer: answer, question: question).correct).to be_false }
    end
  end

  describe 'setting user question result' do
    examples = [
        { attempts: %w(0 0 0 0 1), result: 25 },
        { attempts: %w(1 0 0 0 0 1), result: 25 },
        { attempts: %w(1 0 0 0 0), result: 0 },
        { attempts: %w(1 1 1 1), result: 100 },
        { attempts: %w(1 1 1 1 1), result: 100 },
        { attempts: %w(0 0 0 0 1 1 1 1), result: 100 },
        { attempts: %w(0 0 0 0 1 1 0), result: 25 },
        { attempts: %w(0 0 0 0 1 0 0 0 1), result: 25 }
    ]

    let(:question) { create :question }
    let(:user) { create :user }

    examples.each do |example|
      context "with attempts #{example[:attempts]}" do
        before do
          example[:attempts].each do |correct|
            factory = correct == '1' ? :correct_user_answer : :incorrect_user_answer
            create factory, user: user, question: question
          end
        end

        let(:user_result) { UserQuestionResult.where(user: user, question: question) }

        it 'should have one user question result' do
          expect(user_result.count).to eq 1
        end

        it "should have result #{example[:result]}%" do
          expect(user_result.first.percentage).to eq example[:result]
        end
      end
    end
  end
end
