require 'spec_helper'

describe Merit::PointRules do
  subject { Merit::PointRules.new }

  describe 'check_answers_correct_proc' do
    let!(:user) { create :user }
    let(:category) { create :category }
    let!(:questions) { create_list :question, 20, category: category}
    let(:user_answer) { create :correct_user_answer, user: user, question: questions.first }
    let(:answers_count) { AppSettings.Points_restrictions.correct_answers_for_points }
    subject { Merit::PointRules.new.check_answers_correct_proc.call(user_answer) }

    context 'when few answers given' do
      it { should be_false }
    end

    context 'when there are only incorrect answers' do
      let(:user_answer) { create :incorrect_user_answer, user: user, question: questions.first }
      it { should be_false }
    end

    context 'when enough correct answers given' do
      before { create_answers answers_count-1 }
      it { should be_true}
    end

    context 'when enough answers with repeats within same day' do
      before do
        create :incorrect_user_answer, user: user, question: questions[1]
        create_answers answers_count-1
      end
      it { should be_false }
    end

    context 'when enough answers with repeates during other days' do
      before do
        create :incorrect_user_answer, user: user, question: questions[1], updated_at: Time.now.advance(days: -1)
        create_answers answers_count-1
      end

      it { should be_true }
    end

    context 'when there were incorrect answers' do
      before do
        create :incorrect_user_answer, user: user, question: questions[1]
        create_answers answers_count-2, from: 2
      end

      it { should be_false }
    end

    context "when giving answer for the #{AppSettings.Points_restrictions.correct_answers_for_points+1}-th question" do
      before do
        create_answers answers_count
      end
      let(:user_answer) { create :correct_user_answer, user: user, question: questions[answers_count+1] }

      it { should be_false }
    end

    context "when giving answer for the #{AppSettings.Points_restrictions.correct_answers_for_points*2}-th question" do
      before do
        create_answers answers_count * 2 - 1
      end
      let(:user_answer) { create :correct_user_answer, user: user, question: questions[answers_count * 2] }

      it { should be_true }
    end
  end

  def create_answers(count, params = {})
    from = params[:from] || 2
    trait = params[:trait] || :correct
    questions[(from-1)..(count + from - 2)].each do |q|
      create(:user_answer, trait, {user: user, question: q}.merge(params.except(:trait, :from)))
    end
  end
end
