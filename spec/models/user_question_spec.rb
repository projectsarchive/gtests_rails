require 'spec_helper'

describe UserQuestion do
  describe '.user' do
    let(:user) { create :user }
    let!(:question) { create :question }
    subject { UserQuestion.user(user).first.percentage }

    context 'when result (25) exists' do
      let!(:user_question_result) { create :user_question_result, user: user, question: question, percentage: 25 }
      it { should eq 25.0 }
      it("should mark it started") { expect(UserQuestion.user(user).first.started).to be_true }

      context 'when results for other users exist (40) ' do
        let(:user2) { create :user }
        let!(:user_question_result2) { create :user_question_result, user: user2, question: question, percentage: 40 }

        it { should eq 25.0 }
        it('should have one result per question') { expect(UserQuestion.user(user).count(:all)).to eq 1 }

        describe 'for another user' do
          subject { UserQuestion.user(user2).first.percentage }
          it { should eq 40.0 }
        end
      end
    end

    context 'when attempts exist and result 0' do
      let!(:user_question_result) { create :user_question_result, user: user, question: question, percentage: 0 }

      it("should mark it started") { expect(UserQuestion.user(user).first.started).to be_true }
    end

    context 'when no result' do
      it { should eq 0.0 }
      it("should mark it not started") { expect(UserQuestion.user(user).first.started).to be_false }
    end
  end

  describe 'default scope' do
    subject { UserQuestion.all }
    context 'uses question order field for ordering' do
      let!(:question1) { create :question, order: 2, disk: nil }
      let!(:question2) { create :question, order: 1, disk: nil }

      its('first.id') { should eq question2.id }
      its('last.id') { should eq question1.id }
      its(:count) { should eq 2 }
    end

    context 'skips duplicates' do
      let!(:question) { create :question }
      let!(:duplicate_question) { create :question, duplicate_of: question }

      its('count') { should eq 1 }
      its('first.id') { should eq question.id }
    end
  end
end
