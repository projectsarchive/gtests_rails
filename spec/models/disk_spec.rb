require 'spec_helper'

describe Disk do
  describe 'sorting questions' do
    let(:disk) { FactoryGirl.create :disk_with_questions, questions_count: 3 }

    context 'when no order specified' do
      it 'should order questions by ids' do
        expect(disk.questions.first.id).to be < disk.questions.last.id
      end
    end

    context 'when order specified' do
      it 'should order questions by order' do
        first_question = disk.questions.first
        disk.questions.each_with_index { |q, i| q.update_attribute(:order, 3 - i) }
        disk.reload
        expect(disk.questions.last).to eq first_question
      end
    end
  end
end
