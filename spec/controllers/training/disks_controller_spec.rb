require 'spec_helper'

describe Training::DisksController do
  let!(:category) { create :category }
  let!(:disk) { create :disk_with_questions, category: category }
  let!(:disk_wo_questions) { create :disk, category: category }
  let(:question) { disk.questions.first }
  let(:answer) { question.answers.first }

  describe 'GET #index' do
    it 'not showing disks without questions' do
      visit_index_path
      expect(assigns(:disks)).to eq [disk]
    end

    it 'showing all disks with questions' do
      disk2 = create :disk_with_questions, category: category
      visit_index_path
      expect(assigns(:disks)).to eq [disk, disk2]
    end
  end

  context 'when logged in user' do
    let(:user) { create :user }
    before(:each) { sign_in user }

    describe 'GET #show' do
      it 'redirects to the first unanswered question' do
        visit_show_path
        response.should redirect_to test_category_disk_path(disk.category, disk, 1)
      end

      context 'when disk has no answers' do
        it 'renders show view' do
          visit_show_path disk_wo_questions
          expect(response).to render_template("training/disks/show")
        end
      end
    end

    describe 'GET #test' do
      it 'renders first question' do
        visit_test_path disk, 1
        expect(assigns(:question)).to eq disk.questions[0]
        expect(response.status).to eq 200
      end

      it 'renders last question' do
        visit_test_path disk, disk.questions.count
        expect(assigns(:question)).to eq disk.questions.last
        expect(response.status).to eq 200
      end

      it 'redirects to the last question for higher index' do
        visit_test_path disk, disk.questions.count + 1
        expect(response).to redirect_to test_path(disk, disk.questions.count)
      end

      it 'redirects to the first question for lower index' do
        visit_test_path disk, 0
        expect(response).to redirect_to test_path(disk, 1)
      end

      it 'redirects to #show if no questions in the disk' do
        visit_test_path disk_wo_questions, 1
        expect(response).to redirect_to category_disk_path(category, disk_wo_questions)
      end

      context "when user_answer without attempt_id exists" do
        let!(:another_user_answer) { create :user_answer, question: question, user: user, answer: answer }
        it "initialize new user_answer" do
          visit_test_path disk, 1
          expect(assigns(:user_answer)).to_not eq another_user_answer
        end
        it "its user_answer has user_attempt" do
          visit_test_path disk, 1
          expect(assigns(:user_answer).user_attempt).to_not be_nil
        end
      end
    end

    describe 'POST #test_answer' do
      it 'saves correct user attempt' do
        expect { post_test_path }.to change{UserAnswer.count}.by(1)
        expect(UserAnswer.last.answer_id).to eq answer.id
      end

      it 'redirects to the next question' do
        post_test_path
        expect(response).to redirect_to test_path(disk, 2)
      end

      context 'when skipped previous questions' do
        it 'redirects to the next question' do
          post_test_path 2
          expect(response).to redirect_to test_path(disk, 3)
        end

        context 'and answering last question' do
          it 'redirects to the first unanswered' do
            post_test_path disk.questions.size
            expect(response).to redirect_to test_path(disk, 1)
          end
        end
      end

      shared_examples 'user answer with error' do
        it 'user answer not added' do expect{subject}.to_not change{UserAnswer.count} end
        it 'renders same question' do
          subject
          expect(response.status).to eq 200
          expect(assigns(:question)).to eq disk.questions.first
        end
        it 'sets flash error message' do subject ; expect(flash[:error]).to_not be_nil end
      end

      context 'when no user answer' do
        subject {post_test_path 1, nil}
        it 'raises error' do expect{subject}.to raise_error end
      end

      context 'when answer of another question' do
        subject {post_test_path(1, nil, 111111)}
        it_should_behave_like 'user answer with error'
      end

      context 'when answering last question' do
        let(:attempt) { create :user_attempt, disk: disk,  user: user, completed_at: nil }
        before do
          attempt.disk.questions.drop(1).each do |q|
            create :user_answer, user_attempt: attempt, question: q, user: user
          end
        end

        it 'redirects to finish' do
          post_test_path(1, 1)
          expect(response).to redirect_to finish_category_disk_path category, disk
        end
      end
    end

    describe 'GET #finish' do
      subject { get :finish, category_id: category.slug, id: disk.id }

      shared_examples 'not creating attempts' do
        it 'does not creates new attempt' do
          expect{subject}.to_not change{UserAttempt.count}
        end
      end

      context 'when there is an active attempt' do
        let!(:attempt) { create :user_attempt, disk: disk, user: user, completed_at: nil }
        it 'calls finish on the attempt' do
          expect_any_instance_of(UserAttempt).to receive(:finish)
          subject
        end
        it 'uses correct user attempt' do
          create :user_attempt, disk: disk, user: user, completed_at: Time.now
          subject

          expect(assigns(:attempt)).to eq attempt
        end
        it 'redirects to attempt page with finish flag' do
          subject
          expect(response).to redirect_to attempt_category_disk_path(category, disk, attempt, finish: true)
        end
        it_should_behave_like 'not creating attempts'
      end

      context 'when all finished attempts' do
        let!(:attempt) { create :user_attempt, disk: disk, user: user, completed_at: Time.now }
        it 'redirects to history' do
          subject
          expect(response).to redirect_to history_category_disk_path(category, disk)
        end
        it_should_behave_like 'not creating attempts'
      end

      context 'when no attempts' do
        it 'redirects to the first question' do
          subject
          expect(response).to redirect_to test_category_disk_path(category, disk, 1)
        end
        it_should_behave_like 'not creating attempts'
      end
    end

    describe 'GET #attempt' do
      let(:attempt) { create :user_attempt, user: user, disk: disk }
      subject { get :attempt, category_id: disk.category_id, id: disk.id, attempt_id: attempt.id}

      before { subject }

      it { expect(assigns(:user)).to be_nil }
      it { expect(assigns(:attempt)).to eq attempt }

      context 'when requesting attempt of another user' do
        let(:wrong_attempt) { create :user_attempt, disk: disk }
        subject { get :attempt, category_id: disk.category_id, id: disk.id, attempt_id: wrong_attempt.id}

        it { expect(assigns(:user)).to eq wrong_attempt.user }
      end
    end
  end

  context 'when not logged in user' do
    describe('GET #show') do
      before { visit_show_path }
      it_should_behave_like 'requires login'
    end
    describe('GET #test') do
      before { visit_test_path }
      it_should_behave_like 'requires login'
    end
    describe('POST #test_answer') do
      before { post_test_path }
      it_should_behave_like 'requires login'
    end
    describe('GET #attempt') do
      before { get :attempt, category_id: disk.category_id, id: disk.id, attempt_id: 1 }
      it_should_behave_like 'requires login'
    end
  end

  def visit_show_path (d = disk)
    get :show, category_id: d.category.id, id: d.id
  end

  def visit_index_path
    get :index, category_id: category.id
  end

  def visit_test_path (d = disk, q = 1)
    get :test, category_id: d.category.id, id: d.id, question_number: q
  end

  def post_test_path (q = 1, a_number = 1, a_id = nil)
    a_id ||= disk.questions[q - 1].answers[a_number - 1].id
    user_answer = a_id ? {answer_id: a_id} : nil
    post :test_answer, category_id: disk.category.id, id: disk.id, question_number: q, user_answer: user_answer
  end

  def test_path(d = disk, q = 1)
    test_category_disk_path(d.category, d, q)
  end
end
