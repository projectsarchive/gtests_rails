require 'spec_helper'

describe Training::QuestionsController do
  let(:category) { create :category }
  let(:question) { create :question, category: category }

  let(:user) { create :user }
  before { sign_in user }

  describe "GET 'index'" do
    it "returns http success" do
      get_cat 'index'
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "does not create user answer" do
      expect{get_cat('show', id: question)}.to_not change {UserAnswer.count}
    end
  end

  private

  def get_cat action, params={}
    params.merge! category_id: category.id
    get action, params
  end

  def answer_question question, answer, params={}
    params.merge! category_id: category.id, id: question.id, user_answer: { answer_id: answer.id }
    post 'answer', params
  end

end
