shared_examples 'question view' do
  let(:question) { create :question_with_disk }
  let(:question_view_info) { QuestionViewInfo.new(question) }
  before { assign(:question_view_info, question_view_info) }
  before { assign(:question, question_view_info.question) }

  it('renders question text') { render; expect(rendered).to have_text question.text }
  it('renders all answers') { render ; question.answers.each { |a| expect(rendered).to have_text a.text } }

  context 'with image' do
    before { question_view_info.question = create(:question_with_disk, image: 'test_img.png') }
    before { assign(:question, question_view_info.question) }
    it 'renders image' do
      render

      expect(rendered).to have_selector "img[src='#{File.join(AppSettings.IMG_PATH, 'test_img.png')}']"
    end
  end

  it 'not render image' do
    render
    expect(rendered).to_not have_selector "img"
  end

  describe 'when explanation' do
    context 'not present' do
      it('does not have "Подсказка к вопросу"') { render ; expect(rendered).to_not have_text 'Подсказка к вопросу' }
    end
    context 'present' do
      let(:explanation_body) { '<p>Тестовая подсказка к вопросу</p><img src="test_image.png"/>' }
      before { question_view_info.question = create(:question_with_disk, explanation: explanation_body) }
      before { assign(:question, question_view_info.question) }

      it('has "Подсказка к вопросу"') { render ; expect(rendered).to have_text 'Подсказка к вопросу' }
      it('include html from explanation body') { render ; expect(rendered).to have_css 'img[src="test_image.png"]' }
      it('has explanation body') { render ; expect(rendered).to have_text 'Тестовая подсказка к вопросу' }
    end
  end

  context 'when admin user' do
    before { view.stub(:admin?).and_return(true) }

    it('has link to edit question') do
      render
      expect(rendered).to have_css("a[href~='#{edit_admin_question_path(question)}']")
    end
  end

end
