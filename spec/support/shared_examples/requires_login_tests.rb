shared_examples 'requires login' do
  it 'redirects to new_user_session_path' do
    response.should redirect_to new_user_session_path
  end
end
