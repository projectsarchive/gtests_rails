shared_examples 'names' do
  describe 'full_name should return' do
    before :each do
      @entity = described_class.new
      @entity.first_name = 'Test'
      @entity.last_name = 'User'
      @entity.nickname = 'Nickname'
      @entity.email = 'email@example.com'
    end
    it 'first and last name if present' do
      @entity.full_name.should eq 'Test User'
    end
  
    it 'nickname if names are empty' do
      @entity.first_name = nil
      @entity.last_name = nil
  
      @entity.full_name.should eq 'Nickname'
    end
  
    it 'email if neither both names nor nickname present' do
      @entity.first_name = nil
      @entity.last_name = nil
      @entity.nickname = nil
  
      @entity.full_name.should eq 'email@example.com'
    end
  
    it 'Безымянный if there are no info' do
      entity = described_class.new
  
      entity.full_name.should eq 'Безымянный'
    end
  end
end