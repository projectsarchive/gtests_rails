require 'spec_helper_dbcleaner'
include Warden::Test::Helpers

shared_examples 'non-admin' do
  it { should_not have_link 'Админ' }
  context 'visits admin path' do
    it { should_not have_text 'Панель администрирования' }
  end
end

describe 'Main Menu' do
  before(:all) { Warden.test_mode! }
  before(:each) { visit root_path }

  subject { page }

  context 'when unauthorized user' do
    it_behaves_like 'non-admin'
  end

  context 'when non-admin user' do
    before(:each) { login_user FactoryGirl.create(:user)  }

    it_behaves_like 'non-admin'
  end

  context 'when admin user' do
    before(:each) { login_user FactoryGirl.create(:admin) }

    it { should have_link 'Админ' }
    context 'clicks "Админ"' do
      before(:each) { click_link 'Админ' }
      it { should have_text 'Панель управления' }
    end
  end
end

def login_user(user)
  login_as(user, scope: :user)
  visit current_url # Reload page
end
