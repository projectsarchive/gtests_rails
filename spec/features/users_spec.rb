require 'spec_helper_dbcleaner'
include Warden::Test::Helpers

describe 'Users' do``
  before :all do
    #Capybara.current_driver = :selenium
    #Capybara.default_driver = :selenium
    #Capybara.javascript_driver = :selenium

    OmniAuth.config.test_mode = true
    Warden.test_mode!
  end

  context 'when unauthorized' do
    it 'can register with email and password' do
      visit new_user_registration_path
      fill_in 'Email', with: 'test@example.com'
      fill_in 'Пароль', with: '123123123'
      fill_in 'Подтверждение пароля', with: '123123123'
      click_button 'submit-registration'

      expect(page).to have_text 'test@example.com'
    end

    it 'can login with twitter' do
      OmniAuth.config.add_mock(:twitter, {uid: '11111', credentials: {}, info: {nickname: 'Test Nickname'}})
      visit new_user_registration_path

      click_link 'Twitter'

      expect(page).to have_text 'Test Nickname'
    end
  end

  context 'when authorized' do
    context 'using providers' do
      before :each do
        OmniAuth.config.add_mock(:twitter, {uid: '11111', credentials: {}, info: {nickname: 'Test Nickname'}})
        OmniAuth.config.add_mock(:facebook,
                                 {uid: '11111',
                                  credentials: {},
                                  info: {first_name: 'Test', last_name: 'Name'}})
      end

      before :each do
        visit new_user_registration_path

        click_link 'Twitter'
        sleep 1
        #visit root_path
      end

      describe 'with second provider' do
        before :each do
          visit edit_user_registration_path
          click_link 'Подключить Facebook'
        end

        it 'sees info about provider' do
          expect(uri.path).to eq edit_user_registration_path
          expect(page).to have_text 'Facebook как Test Name'
        end

        it 'can add another account of the same provider' do
          OmniAuth.config.mock_auth[:facebook] =
              OmniAuth::AuthHash.new({
                                         :provider => 'facebook',
                                         :uid => '2222',
                                         credentials: {},
                                         info: {nickname: 'Second Facebook'}
                                     })
          click_link 'Подключить Facebook'

          expect(page).to have_text 'Facebook как Second Facebook'
          expect(page).to have_text 'Facebook как Test Name'
        end

        it 'should keep single provider with same account' do
          click_link 'Подключить Facebook'

          expect(Identity.where(provider: 'facebook').count).to eq 1
        end
      end

      describe 'setting password' do
        before :each do
          User.first.update_attributes!({email: 'test@example.com'})
          fill_password 'coolnewpassword123'
          sleep 1
        end

        it 'can set password without current password if it was not set' do
          click_button 'Обновить'

          within '.alert' do
            expect(page).to have_text 'Учетная запись успешно обновлена'
          end
        end

        it 'can login with a new password' do
          fill_in 'Email', with: 'example@example.com'
          click_button 'Обновить'
          login_using_form 'example@example.com', 'coolnewpassword123'

          expect(page).to have_text 'Test Nickname'
        end

        it 'cannot set new password without current one if the letter was set' do
          click_button 'Обновить'
          fill_password 'newnewpass'
          click_button 'Обновить'

          expect(find('#error_explanation')).to have_text 'Текущий пароль не может быть пустым'
        end

        def fill_password password
          visit edit_user_registration_path
          fill_in 'Пароль', with: password
          fill_in 'Подтверждение пароля', with: password
        end
      end

      it 'can delete provider' do
        visit edit_user_registration_path
        click_link 'Отключить'

        expect(uri.path).to eq edit_user_registration_path
        expect(page).not_to have_text 'Twitter как'
      end
    end

    context 'on main form' do
      before :each do
        #Capybara.current_driver = :selenium
        
        user = FactoryGirl.create(:user)
        login_as(user, :scope => :user)
      end

      it 'stays at the same page after update' do
        visit edit_user_registration_path
        click_button 'Обновить'

        expect(uri.path).to eq edit_user_registration_path
      end

      it 'saves entered first and second names' do
        visit edit_user_registration_path
        fill_in 'Имя', with: 'Тест Имя'
        fill_in 'Фамилия', with: 'Тест Фамилия'
        click_button 'Обновить'

        expect(page).to have_text 'Тест Имя'
        expect(page).to have_text 'Тест Фамилия'
      end

      it 'prevents changing password without current password' do
        visit edit_user_registration_path
        #save_and_open_page
        fill_in 'Пароль', with: 'anewpassword123'
        fill_in 'Подтверждение пароля', with: 'anewpassword123'
        click_button 'Обновить'

        within '#error_explanation' do
          expect(page).to have_text 'Текущий пароль не может быть пустым'
        end

      end
    end

  end

  def uri
    URI.parse(current_url)
  end

  def login_using_form(email, password)
    click_link 'Выйти' if page.has_text?('Выйти')
    click_link 'Войти'
    within '#new_user' do
      fill_in 'Email', with: email
      fill_in 'Пароль', with: password
      click_button 'Войти'
    end
  end
end
