require 'spec_helper_dbcleaner'
include Warden::Test::Helpers

describe 'Training: Disks' do
  before(:all) { Warden.test_mode! }
  let!(:category) { FactoryGirl.create :category_with_disks }
  before(:each) { visit category_disks_path(category) }

  subject { page }

  context 'when admin user' do
    before(:each) { login_user FactoryGirl.create(:admin) }

    it { should have_link 'Редактировать категорию', href: edit_admin_category_path(category) }
  end
end

def login_user(user)
  login_as(user, scope: :user)
  visit current_url # Reload page
end
