require 'spec_helper_dbcleaner'
include Warden::Test::Helpers

describe 'Training: Questions' do
  before(:all) { Warden.test_mode! }
  let!(:category) { FactoryGirl.create :category }
  let!(:questions) { create_list :question, 5, category: category }
  let(:user) { create :user }
  before(:each) { visit category_questions_path(category) }
  before(:each) { login_user user }

  subject { page }

  describe 'completed badge' do
    let(:user2) { create :user }
    let(:question) { questions[0] }

    shared_examples 'displays badge' do |progress_value|
      it "displays #{progress_value} progress" do
        expect(page).to have_css ".icons-completion-#{progress_value}"
      end
    end

    context 'when user answered question with 25% completeness' do
      before { create_correct_answer user, question }
      include_examples 'displays badge', 25
      context 'and another user answered it with 50% completeness' do
        before { create_correct_answer user2, question, 2 }
        include_examples 'displays badge', 25
      end
    end

    context 'when user has not answered question' do
      include_examples 'displays badge', '0-new'
      context 'and another user answered it' do
        before { create_correct_answer user2, question }
        include_examples 'displays badge', '0-new'
      end
    end
  end

  describe 'filtering', js: true do
    context 'when question 1 started and question 2 completed' do
      before do
        create :correct_user_answer, user: user, question: questions[0]
        create_list :correct_user_answer, 4, user: user, question: questions[1]
      end

      [
          {filter: 'На изучении', questions: 0},
          {filter: 'Изученные', questions: 1},
          {filter: 'Новые', questions: 2..4}
      ].each do |example|
        describe "filtering '#{example[:filter]}'" do
          before { click_on example[:filter] }

          visible_indexes = example[:questions]
          visible_indexes = visible_indexes.to_a if visible_indexes.is_a? Range
          visible_indexes = [visible_indexes] unless visible_indexes.is_a? Array

          visible_indexes.each do |index|
            it "should have question #{index + 1}" do
              should have_text questions[index].text
            end
          end

          it 'should not have other questions' do
            questions.select.with_index{|_,i| !visible_indexes.include? i}.each do |question|
              expect(page).to_not have_text question.text
            end
          end

        end
      end
    end
  end
end

def login_user(user)
  login_as(user, scope: :user)
  visit current_url # Reload page
end

def create_correct_answer(user, question, count=1)
  create_list :correct_user_answer, count, user: user, question: question
  visit current_url
end
