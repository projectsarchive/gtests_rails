require 'spec_helper'

describe RobotsGenerator do
  after(:all) { Rails.env = 'test' }

  context 'in production env' do
    before(:each) { Rails.env = 'production' }

    it 'reads from config/robots.txt' do
      File.should_receive(:read) { 'Test robots.txt' }
      expect(RobotsGenerator.call(:production)[2][0]).to eq 'Test robots.txt'
    end
  end

  context 'in staging env' do
    before(:each) { Rails.env = 'staging' }

    it 'returns "Disallow: /"' do
      expect(RobotsGenerator.call(:staging)[2][0]).to include('Disallow: /')
    end

    it 'does not read from file' do
      File.should_not_receive(:read)
      RobotsGenerator.call(:staging)
    end
  end
end