require 'spec_helper'

describe "training/questions/show.html.slim" do
  let(:question) {create :question_with_disk}
  let(:question_view_info) { QuestionViewInfo.new(UserQuestion.user(user).where(category: question.category).first) }
  let(:user) { create :user }

  before do
    assign(:question_view_info, question_view_info)
    assign(:user_answer, create(:user_answer, user: user, question: question))
  end

  it "renders question text" do
    render ; expect(rendered).to have_text question.text
  end

  it "does not show comments" do
    render ; expect(rendered).to_not have_css '#disqus_thread'
  end

end
