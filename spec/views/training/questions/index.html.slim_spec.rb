require 'spec_helper'

describe "training/questions/index.html.slim" do
  describe "setting completion icon" do
    # [percentage, started, badge_postfix]
    examples = [
        [0, false, '0-new'],
        [0, true, 0],
        [20, true, 0],
        [25, true, 25],
        [49, true, 25],
        [50, true, 50],
        [70, true, 50],
        [75, true, 75],
        [99, true, 75],
        [100, true, 100],
    ]

    examples.each do |example|
      context "when #{example[0]}% completed and question #{'not ' if example[1] == 0}started" do
        let(:question) { create :question_with_disk }
        before do
          allow_any_instance_of(Question).to receive(:percentage).and_return(example[0])
          allow_any_instance_of(Question).to receive(:started).and_return(example[1])
          allow_any_instance_of(Question).to receive(:last_answered)
          assign(:questions, Kaminari.paginate_array([question]).page(1))
          assign(:category, question.category)
        end
        it "should have 'icons-completion-#{example[2]}' class" do
          render ; expect(rendered).to have_css ".icons-completion-#{example[2]}"
        end
      end
    end

  end
end
