require 'spec_helper'

describe "training/questions/view.html.slim" do
  let(:question) {create :question_with_disk}
  let(:user) { create :user }
  let!(:other_questions) {create_list :question_with_disk, 10, category: question.category}
  let(:question_view_info) { QuestionViewInfo.new(UserQuestion.user(user).where(category: question.category).first) }

  before do
    assign(:question_view_info, question_view_info)
  end

  context "when answer_id provided" do
    let(:answer) { question.answers.first }
    before { question_view_info.answer = answer }

    it("renders comments") { render ; expect(rendered).to have_css '#disqus_thread' }
    it "has link to next question" do
      render
      expect(rendered).to have_css "a[href='#{category_question_path(question.category, other_questions.first)}']"
    end
  end

  context "when answering last question" do
    let(:last_question) { UserQuestion.user(user).where(category: question.category).last }
    before { question_view_info.question = last_question }

    it("displays link to the first question") do
      render
      expect(rendered).to have_css "a[href='#{category_question_path(last_question.category, question)}']"
    end
  end

end
