require 'spec_helper'

describe 'training/disks/view.html.slim' do
  let(:question) { create :question_with_disk }
  let(:question_view_info) { QuestionViewInfo.new(question) }
  before { assign(:question_view_info, question_view_info) }

  it_behaves_like 'question view'

  it('marks correct answer') { render ; expect(rendered).to have_css('.correct-answer') }

  context 'when admin user' do
    before { view.stub(:admin?).and_return(true) }

    it('does not have link to view question') do
      render
      expect(rendered).to_not have_css("a[href~='#{view_disk_question_path(question)}']")
    end

    it('has link to test question') do
      render
      expect(rendered).to have_css("a[href~='#{answer_disk_question_path(question)}']")
    end
  end

end
