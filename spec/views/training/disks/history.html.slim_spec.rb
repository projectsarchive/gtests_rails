require 'spec_helper'

describe 'training/disks/history.html.slim' do
  let(:disk) { create(:disk) }
  before { assign(:disk, disk) }

  context 'with attempts present' do
    let(:attempts) {create_list(:user_attempt, 4)}
    before { assign(:attempts, attempts) }

    it 'renders all attempts' do
      render

      expect(rendered).to have_selector 'table tr', count: 5
    end

    it 'has links to attempt detail page' do
      render
      expect(rendered).to have_css "a[href~='#{attempt_path(attempts.first)}']"
    end
  end

  context 'without attempts' do
    it 'has link to start attempt' do
      render
      expect(rendered).to have_css "a[href~='#{test_category_disk_path(disk.category, disk, 1)}']"
    end
  end
end
