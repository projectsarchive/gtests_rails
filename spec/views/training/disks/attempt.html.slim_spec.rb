require 'spec_helper'

describe 'training/disks/attempt.html.slim' do
  let(:user) { create :user }
  let(:attempt) { create :user_attempt_with_answers, user: user }
  let(:answer) { attempt.user_answers.first }

  before { assign(:attempt, attempt) }

  it 'renders all questions' do
    render
    expect(rendered).to have_selector('table tr', count: attempt.disk.questions.count + 1)
  end

  it 'not renders info of current user' do
    render
    expect(rendered).to_not have_text "Результаты пользователя #{user.full_name}"
  end

  context 'when attempt of another user' do
    before { assign(:user, user) }
    it 'renders info of that user' do
      render
      expect(rendered).to have_text "Результаты пользователя #{user.full_name}"
    end
  end
end
