require 'spec_helper'

describe 'training/disks/test.html.slim' do
  let(:attempt) {create :user_attempt}
  let(:disk) { create :disk_with_questions }
  let(:question) { disk.questions.first }
  before(:each) do
    controller.request.path_parameters[:category_id] = disk.category.slug
    controller.request.path_parameters[:id] = disk.id
    assign(:user_answer, create(:user_answer, user_attempt: attempt))
    assign(:question, question)
  end

  it_behaves_like 'question view'

  context 'for the first question' do
    it 'form has correct action' do
        render
        assert_select "form[action*=?]", "disks/#{disk.id}/1"
    end
  end

  context 'when admin user' do
    before { view.stub(:admin?).and_return(true) }

    it('has link to view question') do
      render
      expect(rendered).to have_css("a[href~='#{view_disk_question_path(question)}']")
    end

    it('does not have link to test question') do
      render
      expect(rendered).to_not have_css("a[href~='#{answer_disk_question_path(question)}']")
    end
  end
end
