# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    password 'somerandompassword'
    password_confirmation 'somerandompassword'
    sequence(:email) { |n| "john.doe_#{n}@example.com" }
  end

  factory :admin, parent: :user do
    admin true
  end
end
