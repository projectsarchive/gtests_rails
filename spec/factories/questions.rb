# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :question do
    ignore do
      answers_count 4
    end

    sequence(:text) { |n| "MyQuestion #{n}" }
    image nil
    code {rand(10000..99999).to_s}
    order 1
    disk nil
    category { disk ? disk.category : create(:category) }

    after(:create) do |question, evaluator|
      answers = build_list(:answer, evaluator.answers_count, question: question)
      answers.first.correct = true
      question.answers = answers
    end

    factory :question_with_disk do
      disk
      category { disk.category if disk }
    end
  end
end
