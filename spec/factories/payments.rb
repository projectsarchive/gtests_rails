# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payment do
    notification_type "MyString"
    operation_id "MyString"
    amount 1.5
    withdraw_amount 1.5
    datetime "2014-04-20 12:37:37"
    sender "MyString"
    codepro false
    label "MyString"
    user nil
    points 1.5
  end
end
