# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_attempt do
    user
    association :disk, factory: :disk_with_questions
    completed_at "2014-02-02 15:37:02"

    factory :user_attempt_with_answers do
      ignore do
        answers_count 1
        correct_answers 1
      end

      before(:create) do |attempt, evaluator|
        attempt.correct_count = evaluator.correct_answers
      end

      after(:create) do |attempt, evaluator|
        correct = evaluator.correct_answers
        [attempt.disk.questions.count, evaluator.answers_count].min.times do |i|
          trait = correct > 0 ? :correct : :incorrect
          correct -= 1
          attempt.user_answers << create(
              :user_answer,
              trait,
              user: attempt.user,
              user_attempt: attempt,
              question: attempt.disk.questions[i]
          )
        end
      end
    end
  end
end
