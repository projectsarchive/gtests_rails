# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :disk do
    sequence(:name) { |n| "Disk #{n}" }
    code { rand(100000..999999).to_s }
    category

    factory :disk_with_questions do
      ignore do
        questions_count 14
      end

      after(:create) do |disk, evaluator|
        questions = FactoryGirl.create_list(:question, evaluator.questions_count, disk: disk)
        disk.questions = questions
      end
    end
  end
end
