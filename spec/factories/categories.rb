# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :category do
    sequence(:slug) { |n| "category_#{n}" }

    name "MyString"
    description "MyText"
    code "MyString"
    full_description "MyText"
    seo_title "MyString"
    seo_description "MyText"
    seo_keywords "MyString"

    factory :category_with_disks do
      after(:create) do |category, disks_count=10|
        disks = FactoryGirl.create_list(:disk, disks_count, category: category)
        category.disks = disks
      end
    end
  end
end
