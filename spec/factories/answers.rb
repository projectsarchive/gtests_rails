# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :answer do
    question
    sequence(:text) { |n| "MyAnswer #{n}" }
    correct false
    code { rand(100000..9999999).to_s }
    order 1
  end
end
