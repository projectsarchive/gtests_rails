# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_answer do
    user
    question
    answer { question.answers.sample }
    user_attempt nil

    trait :correct do
      answer { question.answers.where(correct: true).sample }
    end

    trait :incorrect do
      answer { question.answers.where(correct: false).sample }
    end

    factory :correct_user_answer, traits: [:correct]
    factory :incorrect_user_answer, traits: [:incorrect]
  end
end
