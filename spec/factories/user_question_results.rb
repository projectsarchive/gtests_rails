# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user_question_result do
    question nil
    user nil
    percentage 1.5
  end
end
